#!/bin/bash -l
# Grid Engine options (lines prefixed with #$)
#$ -N Athoy_LZ             
#$ -cwd                  
#$ -l h_rt=24:00:00 
#$ -l s_rt=24:00:00 

#  These options are:
#  job name: -N
#  use the current working directory: -cwd
#  runtime limit of 5 minutes: -l h_rt
#  memory limit of 1 Gbyte: -l h_vmem
# Initialise the environment modules
. /etc/profile.d/modules.sh
#Normal bash commands here:
#NAME=Single_Particle_gamma

ENERGY=$1

#for SEED in `seq 100400 1 100449`; do
#for SEED in `seq 100300 1 100349`; do
#for SEED in `seq 100200 1 100249`; do
#for SEED in `seq 100080 1 100099`; do
#for SEED in `seq 100100 1 100149`; do
for SEED in `seq 100000 1 100499`; do
        qsub Run_Mac.sh $SEED $ENERGY
#	 sh Run_Mac.sh $SEED $ENERGY
done

