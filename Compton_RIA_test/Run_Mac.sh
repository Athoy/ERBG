#!/bin/bash -l
# Grid Engine options (lines prefixed with #$)
#$ -N Athoy_LZ             
#$ -cwd

#$ -l h_rt=4:00:00 
#$ -l s_rt=4:00:00 
		#	# -m beas
#  These options are:
#  job name: -N
#  use the current working directory: -cwd
#  runtime limit of 5 minutes: -l h_rt
#  memory limit of 1 Gbyte: -l h_vmem
# Initialise the environment modules
. /etc/profile.d/modules.sh

#Normal bash commands here:
SEED=$1
ENERGY=$2
NAME=Single_Particle_gamma

source /home/s1642680/LZ_Stuff/ERBG/Compton_RIA_test/set_env.sh
$BACCTOOLS/bin/BACCARATExecutable Macros/${NAME}_${ENERGY}_${SEED}.mac
$BACCTOOLS/bin/BaccRootConverter Outputs/Out_${NAME}_${ENERGY}_${SEED}.bin
rm ./Outputs/Out_${NAME}_${ENERGY}_${SEED}.bin


#mkdir -p /scratch/anilima
#mkdir -p /scratch/anilima/Outputs/
#mkdir -p /scratch/anilima/Outputs/${NAME}_${ENERGY}
#scp -r /home/s1642680/LZ_Stuff/ERBG/Compton_RIA_test/Outputs/Out_${NAME}_${ENERGY}_${SEED}.root /scratch/anilima/Outputs/Out_${NAME}_${ENERGY}/
