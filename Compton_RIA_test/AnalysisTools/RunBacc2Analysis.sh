#!/bin/bash -l

source $(pwd)/../set_env.sh

BACCANALYSISTREEDIR=/cvmfs/lz.opensciencegrid.org/TDRAnalysis/release-6.0.0/ReducedAnalysisTree/
#BACCANALYSISTREEDIR=../../../TDRAnalysis/ReducedAnalysisTree/
ENERGY=$1
MODEL=$2
NAME=Single_Particle_gamma
#INPDIR=input/$NAME
INPDIR=/scratch/anilima/Outputs/Out_${ENERGY}_${MODEL}/

#Analysis Phase

mkdir -p /scratch/anilima/AnaTreeResults
mkdir -p /scratch/anilima/AnaTreeResults/${NAME}_${ENERGY}_${MODEL}

for FILE in $INPDIR/Out_${NAME}_${MODEL}_${ENERGY}_merged.root; do
	FILENAME="$(basename $FILE .root)"	

	echo "Running Analysis for " $FILE
	$BACCANALYSISTREEDIR/Bacc2AnalysisTree  $FILE
	OUTNAME=${FILENAME}_analysis_tree.root
	mv $OUTNAME /scratch/anilima/AnaTreeResults/${NAME}_${ENERGY}_${MODEL}/

done

