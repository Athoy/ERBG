#!/bin/bash -l
source /cvmfs/lz.opensciencegrid.org/LzBuild/release-latest/setup.sh
MODEL=$1
mkdir AnaTree_Cut_Results/${MODEL}_UKDC/
if [ "$MODEL" == "Monash" ]; then
	IN_PATH=BACCARAT_dev-monash-cbfc0dac_geant10.3/reduced_v6.0.0
	COMPLIST=("lz_tpc_pmts_Co60_g4decay")
	#COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_U238_g4decay" "lz_vessels_Co60_g4decay")
elif [ "$MODEL" == "Livermore" ]; then
	IN_PATH=BACCARAT_release-4.0.0_geant10.3/reduced_v6.0.0
	COMPLIST=("lz_tpc_pmts_Co60_g4decay")	
	#COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_U238_g4decay" "lz_vessels_Co60_g4decay")
else
	exit 1
fi
echo "MODEL= " $MODEL
for COMP in ${COMPLIST[*]}; do
	mkdir AnaTree_Cut_Results/${MODEL}_UKDC/${COMP}/
	OUT_PATH=AnaTree_Cut_Results/${MODEL}_UKDC/${COMP}/
	IN=${IN_PATH}/${COMP}
	OUT=${OUT_PATH}/
	LISTNAME=${LISTPATH}${COMP}.txt
	./bg_analysis_UKDC.exe --UKDC --units keVee --outfile ${OUT}/${COMP}_Cut "name:${COMP};inputs:$IN/*.root"
	mv *${COMP}*png *${COMP}*pdf bg_analysis_*.txt ${OUT}/
done

