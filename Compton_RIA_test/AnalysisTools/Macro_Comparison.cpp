//========================================================================//

// Written by : Athoy Nilima June 19, 2018
//========================================================================//
// Apply analysis cuts to reduced analysis tree produced by
// Bacc2AnalysisTree.cc.
// To compile:
//   g++ -o BG_analysis.exe BG_analysis.cc `root-config --cflags --glibs`
//
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TBranch.h"
#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCutG.h"
#include "TCut.h"
#include "TEntryList.h"
#include "TPaveLabel.h"
#include "TLegend.h"
#include "TObjArray.h"
#include "TChainElement.h"
#include "TRegexp.h"
#include "TSystem.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TText.h"

#define DEBUG 0
void AddLines(double,double);
void Macro_Comparison(TString,int EN);
void Macro_Comparison(TString NAME,int EN){
    
    //double xmax=100+EN;
    double xmax=100;
    TString ENERGY=to_string(EN);
    gStyle->SetPalette(1);
    gStyle->SetOptStat("ei");
    gStyle->SetStatY(0.9);
    gStyle->SetStatX(0.9);
    gStyle->SetStatW(0.3);
    gStyle->SetStatH(0.15);
    gStyle->SetPadRightMargin(0.1);
    gStyle->SetPadRightMargin(0.15);
    gStyle->SetTitleOffset(1.3,"y");
    cout << endl;

    TString INPFolder="/scratch/anilima/AnaTree_Cut_Results";
    //TString INPFolder="./AnaTree_Cut_Results";
    TString FILE1Name=INPFolder+"/"+NAME+"_"+ENERGY+"_"+"Livermore"+"/Out_"+NAME+"_Livermore_"+ENERGY+"_"+"After_Cut_histos.root";
    TString FILE2Name=INPFolder+"/"+NAME+"_"+ENERGY+"_"+"Monash"+"/Out_"+NAME+"_Monash_"+ENERGY+"_"+"After_Cut_histos.root";
    TString CanvasName="Energy Depositions: Incident Gamma Energy="+to_string(EN);
    TCanvas * c2 = new TCanvas(CanvasName, "", 50, 50, 1000, 800);
    c2->Divide(2,3);
    
    TFile *File1=new TFile(FILE1Name);
    TFile *File2=new TFile(FILE2Name);
    
    // LXe
   
    TString LXe_Histo_Name1="h1d_enedep_lxe_Out_"+NAME+"_Livermore_"+ENERGY;
    TString LXe_Histo_Name2="h1d_enedep_lxe_Out_"+NAME+"_Monash_"+ENERGY;
    
    TH1D *Edep_LXe_1=(TH1D*)File1->Get(LXe_Histo_Name1);
    TH1D *Edep_LXe_2=(TH1D*)File2->Get(LXe_Histo_Name2);
    
    c2->cd(1);
    gPad->SetLogy();
    //gPad->SetLogx();
    
    Edep_LXe_1->SetLineColor(kBlue);
    Edep_LXe_2->SetLineColor(kRed);
    Edep_LXe_1->SetAxisRange(0.,xmax,"X");
    Edep_LXe_2->SetAxisRange(0.,xmax,"X");
    Edep_LXe_1->Draw();
    c2->Update();
    TPaveStats *ps11 = (TPaveStats*)Edep_LXe_1->GetListOfFunctions()->FindObject("stats");
    ps11->SetX1NDC(0.15); ps11->SetX2NDC(0.35);
    ps11->SetTextColor(kBlue);
    c2->Update();
    
    Edep_LXe_2->Draw("SAME");
    c2->Update();
    TPaveStats *ps21 = (TPaveStats*)Edep_LXe_2->GetListOfFunctions()->FindObject("stats");
    ps21->SetX1NDC(0.4); ps21->SetX2NDC(0.6);
    ps21->SetTextColor(kRed);
    c2->Update();
    TLegend *leg1= new TLegend(0.7,0.8,0.7,0.8);
    leg1->AddEntry(Edep_LXe_1,"Livermore Physics","l");
    leg1->AddEntry(Edep_LXe_2,"Monash Physics","l");
    //leg1->SetX1NDC(0.01); leg1->SetX2NDC(0.9);
    //leg1->Draw("SAME ");
    c2->Update();
    
    //Single Scatter
    TString ss_Histo_Name1="h1d_enedep_lxe_ss_Out_"+NAME+"_Livermore_"+ENERGY;
    TString ss_Histo_Name2="h1d_enedep_lxe_ss_Out_"+NAME+"_Monash_"+ENERGY;
    
    TH1D *Edep_ss_1=(TH1D*)File1->Get(ss_Histo_Name1);
    TH1D *Edep_ss_2=(TH1D*)File2->Get(ss_Histo_Name2);
    
    c2->cd(2);
    gPad->SetLogy();
    //gPad->SetLogx();
    
    Edep_ss_1->SetLineColor(kBlue);
    Edep_ss_2->SetLineColor(kRed);
    Edep_ss_1->SetAxisRange(0.,xmax,"X");
    Edep_ss_2->SetAxisRange(0.,xmax,"X");
 
    Edep_ss_1->Draw();
    c2->Update();
    TPaveStats *ps12 = (TPaveStats*)Edep_ss_1->GetListOfFunctions()->FindObject("stats");
    ps12->SetX1NDC(0.15); ps12->SetX2NDC(0.35);
    ps12->SetTextColor(kBlue);
    c2->Update();
    

    Edep_ss_2->Draw("SAME");
    c2->Update();
    TPaveStats *ps22 = (TPaveStats*)Edep_ss_2->GetListOfFunctions()->FindObject("stats");
    ps22->SetX1NDC(0.4); ps22->SetX2NDC(0.6);
    ps22->SetTextColor(kRed);
    c2->Update();
    TLegend *leg2= new TLegend(0.7,0.8,0.7,0.8);
    leg2->AddEntry(Edep_ss_1,"Livermore Physics","l");
    leg2->AddEntry(Edep_ss_2,"Monash Physics","l");
    //leg2->SetX1NDC(0.01); leg2->SetX2NDC(0.9);
    //leg2->Draw("SAME ");
    c2->Update();
    
    
    //OD
    TString od_Histo_Name1="h1d_enedep_lxe_od_Out_"+NAME+"_Livermore_"+ENERGY;
    TString od_Histo_Name2="h1d_enedep_lxe_od_Out_"+NAME+"_Monash_"+ENERGY;
    
    TH1D *Edep_od_1=(TH1D*)File1->Get(od_Histo_Name1);
    TH1D *Edep_od_2=(TH1D*)File2->Get(od_Histo_Name2);
    
    c2->cd(3);
    gPad->SetLogy();
    //gPad->SetLogx();
    
    Edep_od_1->SetLineColor(kBlue);
    Edep_od_2->SetLineColor(kRed);
    Edep_od_1->SetAxisRange(0.,xmax,"X");
    Edep_od_2->SetAxisRange(0.,xmax,"X");
    Edep_od_1->Draw();
    c2->Update();
    TPaveStats *ps13 = (TPaveStats*)Edep_od_1->GetListOfFunctions()->FindObject("stats");
    ps13->SetX1NDC(0.15); ps13->SetX2NDC(0.35);
    ps13->SetTextColor(kBlue);
    c2->Update();
    
    Edep_od_2->Draw("SAME");
    c2->Update();
    TPaveStats *ps23 = (TPaveStats*)Edep_od_2->GetListOfFunctions()->FindObject("stats");
    ps23->SetX1NDC(0.4); ps23->SetX2NDC(0.6);
    ps23->SetTextColor(kRed);
    c2->Update();
    TLegend *leg3= new TLegend(0.7,0.8,0.7,0.8);
    leg3->AddEntry(Edep_od_1,"Livermore Physics","l");
    leg3->AddEntry(Edep_od_2,"Monash Physics","l");
    //leg3->SetX1NDC(0.01); leg3->SetX2NDC(0.9);
    //leg3->Draw("SAME ");
    c2->Update();
    
    //Skin
    TString skin_Histo_Name1="h1d_enedep_lxe_skin_Out_"+NAME+"_Livermore_"+ENERGY;
    TString skin_Histo_Name2="h1d_enedep_lxe_skin_Out_"+NAME+"_Monash_"+ENERGY;
    
    TH1D *Edep_skin_1=(TH1D*)File1->Get(skin_Histo_Name1);
    TH1D *Edep_skin_2=(TH1D*)File2->Get(skin_Histo_Name2);
    
    c2->cd(4);
    gPad->SetLogy();
    //gPad->SetLogx();
    
    Edep_skin_1->SetLineColor(kBlue);
    Edep_skin_2->SetLineColor(kRed);
    Edep_skin_1->SetAxisRange(0.,xmax,"X");
    Edep_skin_2->SetAxisRange(0.,xmax,"X");
    Edep_skin_1->Draw();
    c2->Update();
    TPaveStats *ps14 = (TPaveStats*)Edep_skin_1->GetListOfFunctions()->FindObject("stats");
    ps14->SetX1NDC(0.15); ps14->SetX2NDC(0.35);
    ps14->SetTextColor(kBlue);
    c2->Update();
    Edep_skin_2->Draw("SAME");
    c2->Update();
    TPaveStats *ps24 = (TPaveStats*)Edep_skin_2->GetListOfFunctions()->FindObject("stats");
    ps24->SetX1NDC(0.4); ps24->SetX2NDC(0.6);
    ps24->SetTextColor(kRed);
    c2->Update();
    TLegend *leg4= new TLegend(0.7,0.8,0.7,0.8);
    leg4->AddEntry(Edep_skin_1,"Livermore Physics","l");
    leg4->AddEntry(Edep_skin_2,"Monash Physics","l");
    //leg4->SetX1NDC(0.01); leg4->SetX2NDC(0.9);
    //leg4->Draw("SAME ");
    c2->Update();
    
    //Skin+OD
    TString skin_od_Histo_Name1="h1d_enedep_lxe_skin_od_Out_"+NAME+"_Livermore_"+ENERGY;
    TString skin_od_Histo_Name2="h1d_enedep_lxe_skin_od_Out_"+NAME+"_Monash_"+ENERGY;
    
    TH1D *Edep_skin_od_1=(TH1D*)File1->Get(skin_od_Histo_Name1);
    TH1D *Edep_skin_od_2=(TH1D*)File2->Get(skin_od_Histo_Name2);
     
    c2->cd(5);
    gPad->SetLogy();
    //gPad->SetLogx();
    
    Edep_skin_od_1->SetLineColor(kBlue);
    Edep_skin_od_2->SetLineColor(kRed);
    Edep_skin_od_1->SetAxisRange(0.,xmax,"X");
    Edep_skin_od_2->SetAxisRange(0.,xmax,"X");
    Edep_skin_od_1->SetAxisRange(0.001,100,"Y");
    Edep_skin_od_2->SetAxisRange(0.001,100,"Y");
    Edep_skin_od_1->Draw();
    c2->Update();
    TPaveStats *ps15 = (TPaveStats*)Edep_skin_od_1->GetListOfFunctions()->FindObject("stats");
    ps15->SetX1NDC(0.15); ps15->SetX2NDC(0.35);
    ps15->SetTextColor(kBlue);
    c2->Update();
    
    Edep_skin_od_2->Draw("SAME");
    c2->Update();
    TPaveStats *ps25 = (TPaveStats*)Edep_skin_od_2->GetListOfFunctions()->FindObject("stats");
    ps25->SetX1NDC(0.4); ps25->SetX2NDC(0.6);
    ps25->SetTextColor(kRed);
    c2->Update();
    TLegend *leg5= new TLegend(0.7,0.8,0.7,0.8);
    leg5->AddEntry(Edep_skin_od_1,"Livermore Physics","l");
    leg5->AddEntry(Edep_skin_od_2,"Monash Physics","l");
    //leg5->SetX1NDC(0.01); leg5->SetX2NDC(0.9);
    //leg5->Draw("SAME ");
    c2->Update();
    //Skin+OD, Fid Vol
    TString skin_od_fv_Histo_Name1="h1d_enedep_lxe_skin_od_fv_Out_"+NAME+"_Livermore_"+ENERGY;
    TString skin_od_fv_Histo_Name2="h1d_enedep_lxe_skin_od_fv_Out_"+NAME+"_Monash_"+ENERGY;
    
    TH1D *Edep_skin_od_fv_1= (TH1D*)File1->Get(skin_od_fv_Histo_Name1);
    TH1D *Edep_skin_od_fv_2=(TH1D*)File2->Get(skin_od_fv_Histo_Name2);
    
    c2->cd(6);
    gPad->SetLogy();
    //gPad->SetLogx();
    
    Edep_skin_od_fv_1->SetLineColor(kBlue);
    Edep_skin_od_fv_2->SetLineColor(kRed);
    Edep_skin_od_fv_1->SetAxisRange(0.,xmax,"X");
    Edep_skin_od_fv_2->SetAxisRange(0.,xmax,"X");
    Edep_skin_od_fv_1->Draw();
    c2->Update();
    TPaveStats *ps16 = (TPaveStats*)Edep_skin_od_fv_1->GetListOfFunctions()->FindObject("stats");
    ps16->SetX1NDC(0.15); ps16->SetX2NDC(0.35);
    ps16->SetTextColor(kBlue);
    c2->Update();
    Edep_skin_od_fv_2->Draw("SAME");
    c2->Update();
    TPaveStats *ps26 = (TPaveStats*)Edep_skin_od_fv_2->GetListOfFunctions()->FindObject("stats");
    ps26->SetX1NDC(0.4); ps26->SetX2NDC(0.6);
    ps26->SetTextColor(kRed);
    c2->Update();
    
    TLegend *leg6= new TLegend(0.7,0.8,0.7,0.8);
    leg6->AddEntry(Edep_skin_od_fv_1,"Livermore Physics","l");
    leg6->AddEntry(Edep_skin_od_fv_2,"Monash Physics","l");
    //leg6->SetX1NDC(0.01); leg6->SetX2NDC(0.9);
    //leg6->Draw("SAME ");
    c2->Update();

    TString PLOTNAME="./Comparison_Plots/Comparisons_"+NAME+"_"+ENERGY+".png";
    c2->Print(PLOTNAME);

}
void AddLines(double ymin,double ymax){
    
    //ymin=0;//for log scale
    TLine *line1 = new TLine(0.0134,ymin,0.0134,ymax);
    TLine *line2 = new TLine(0.0233,ymin,0.0233,ymax);
    TLine *line3 = new TLine(0.0675,ymin,0.0675,ymax);
    TLine *line4 = new TLine(0.0695,ymin,0.0695,ymax);
    TLine *line5 = new TLine(0.1455,ymin,0.1455,ymax);
    TLine *line6 = new TLine(0.1467,ymin,0.1467,ymax);
    TLine *line7 = new TLine(0.2132,ymin,0.2132,ymax);
    TLine *line8 = new TLine(0.6764,ymin,0.6764,ymax);
    TLine *line9 = new TLine(0.689,ymin,0.689,ymax);
    TLine *line10 = new TLine(0.9406,ymin,0.9406,ymax);
    TLine *line11 = new TLine(1.0021,ymin,1.0021,ymax);
    TLine *line12 = new TLine(1.1487,ymin,1.1487,ymax);
    TLine *line13 = new TLine(4.786,ymin,4.786,ymax);
    TLine *line14 = new TLine(5.107,ymin,5.107,ymax);
    TLine *line15 = new TLine(5.453,ymin,5.453,ymax);
    TLine *line16 = new TLine(34.561,ymin,34.561,ymax);
    
    line1->SetLineStyle(2);
    line2->SetLineStyle(2);
    line3->SetLineStyle(2);
    line4->SetLineStyle(2);
    line5->SetLineStyle(2);
    line6->SetLineStyle(2);
    line7->SetLineStyle(2);
    line8->SetLineStyle(2);
    line9->SetLineStyle(2);
    line10->SetLineStyle(2);
    line11->SetLineStyle(2);
    line12->SetLineStyle(2);
    line13->SetLineStyle(2);
    line14->SetLineStyle(2);
    line15->SetLineStyle(2);
    line16->SetLineStyle(2);
    
    line1->Draw("SAME");
    line2->Draw("SAME");
    line3->Draw("SAME");
    line4->Draw("SAME");
    line5->Draw("SAME");
    line6->Draw("SAME");
    line7->Draw("SAME");
    line8->Draw("SAME");
    line9->Draw("SAME");
    line10->Draw("SAME");
    line11->Draw("SAME");
    line12->Draw("SAME");
    line13->Draw("SAME");
    line14->Draw("SAME");
    line15->Draw("SAME");
    line16->Draw("SAME");
}


