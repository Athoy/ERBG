Note that ER roi is 0-100keV, and are scaled to 1.5-6.5keV for the final column. NR roi is 6-30keV.

Outputting information in table format with headings: 
 | Component & Source  | Total Events  | Yield (for neutrons)  | N Decays |  Xe EDep + ROI |  + Single Hit |  + LXe Skin | + OD  | + OD + LXe Skin  | 5.6t fiducial | P(survival)<br /> /n or g |  P(survival) /decay  | 1000 days @ 1mBq/kg  |

 | Out_Single_Particle_gamma_Livermore_500 | 1.25e+07 | 1 | 12500 | 4364 | 4332 | 1425 | 3713 | 891 | 43 | 3.44e-06 | 0.00344 | 297305 | 

Old cut order: 
Events in ROI: 4364
Events in ROI + fiducial: 641
Events in ROI + fiducial + single: 631
Events in ROI + fiducial + single + skin: 88
Events in ROI + fiducial + single + OD: 569
Events in ROI + fiducial + single + skin + OD: 43 

