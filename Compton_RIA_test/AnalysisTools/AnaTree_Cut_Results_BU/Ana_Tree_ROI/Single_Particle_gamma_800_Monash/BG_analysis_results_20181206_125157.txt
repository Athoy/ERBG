Note that ER roi is 0-100keV, and are scaled to 1.5-6.5keV for the final column. NR roi is 6-30keV.

Outputting information in table format with headings: 
 | Component & Source  | Total Events  | Yield (for neutrons)  | N Decays |  Xe EDep + ROI |  + Single Hit |  + LXe Skin | + OD  | + OD + LXe Skin  | 5.6t fiducial | P(survival)<br /> /n or g |  P(survival) /decay  | 1000 days @ 1mBq/kg  |

 | Out_Single_Particle_gamma_Monash_800 | 1.25e+07 | 1 | 12500 | 3463 | 3442 | 1284 | 2563 | 590 | 66 | 5.28e-06 | 0.00528 | 456329 | 

Old cut order: 
Events in ROI: 3463
Events in ROI + fiducial: 836
Events in ROI + fiducial + single: 824
Events in ROI + fiducial + single + skin: 183
Events in ROI + fiducial + single + OD: 653
Events in ROI + fiducial + single + skin + OD: 66 

