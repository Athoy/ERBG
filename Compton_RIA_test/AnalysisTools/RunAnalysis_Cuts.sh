#!/bin/bash -l

#source $(pwd)/../set_env.sh

# Sctipt not working with Baccarat settings
#module unload ROOT
#module load ROOT/6.08.00


ENERGY=$1
MODEL=$2
#NAME=$1
NAME=Single_Particle_gamma
INPDIR=/scratch/anilima/AnaTreeResults/${NAME}_${ENERGY}_${MODEL}/

#Analysis Phase

mkdir -p /scratch/anilima/AnaTree_Cut_Results

mkdir -p ./AnaTree_Cut_Results
mkdir -p ./AnaTree_Cut_Results/${NAME}_${ENERGY}_${MODEL}

for FILE in $INPDIR/Out_${NAME}_${MODEL}_${ENERGY}_merged_analysis_tree.root; do
	NEW_FILENAME=${NAME}_${MODEL}_${ENERGY}
	FILENAME="$(basename $FILE _merged_analysis_tree.root)"	
	echo "Applying Analysis Cuts for " $FILE
	./BG_analysis.exe --MaxEDep ${ENERGY} --units keVee --outfile AnaTree_Cut_Results/${NAME}_${ENERGY}_${MODEL}/${FILENAME}_After_Cut "name:${FILENAME};inputs:${FILE};mass:7000;activity:0.1429"	
        #./BG_analysis.exe --savetree --units keVee --outfile AnaTree_Cut_Results/${NAME}_${ENERGY}_${MODEL}/${FILENAME}_After_Cut "name:${FILENAME};inputs:${FILE}"
	mv Out_${NAME}_${MODEL}_${ENERGY}*png Out_${NAME}_${MODEL}_${ENERGY}*pdf BG_analysis_*.txt AnaTree_Cut_Results/${NAME}_${ENERGY}_${MODEL}/
	#mv AnaTree_Cut_Results/${NAME}_${ENERGY}_${MODEL} /scratch/anilima/AnaTree_Cut_Results/
done

