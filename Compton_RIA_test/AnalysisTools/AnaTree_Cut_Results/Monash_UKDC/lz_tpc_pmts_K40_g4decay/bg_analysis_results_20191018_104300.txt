Note that ER roi is 0-100keV, and are scaled to 1.5-6.5keV for the final column. NR roi is 6-30keV.

Outputting information in table format with headings: 
 | Component & Source  | Total Events  | Yield (for neutrons)  | N Decays |  Xe EDep + ROI |  + Single Hit |  + LXe Skin | + OD  | + OD + LXe Skin  | 5.6t fiducial | P(survival)<br /> /n or g |  P(survival) /decay  | 1000 days @ 1mBq/kg  |
