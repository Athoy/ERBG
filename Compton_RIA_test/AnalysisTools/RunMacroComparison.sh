#!/bin/bash -l
#SBATCH -t 6:00:00 --ntasks=1 --account lz
#SBATCH -J AthoyPP -p shared-chos
#SBATCH --output=Job_Out/Job.%j.out
#SBATCH --error=Job_Out/Job.%j.err
#SBATCH --mem=8G
#Normal bash commands here:


# Sctipt not working with Baccarat settings

#module load ROOT/6.08.00

NAME=Single_Particle_gamma
mkdir -p ./Comparison_Plots

ENERGYLIST="100 200 300 400 500 600 700 800 900 1000"
#ENERGY=$1
#NAME=$2
for ENERGY in $ENERGYLIST; do
	root -b -q -l "Macro_Comparison.cpp(\"$NAME\",$ENERGY)"
done
