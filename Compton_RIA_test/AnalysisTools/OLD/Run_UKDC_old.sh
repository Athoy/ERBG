#!/bin/bash -l
MODEL=$1
mkdir AnaTree_Cut_Results/${MODEL}_UKDC/
if [ "$MODEL" == "Monash" ]; then
	#IN_PATH=/scratch/anilima/UKDC/BACCARAT_dev-monash-cbfc0dac_geant10.3/reduced_v6.0.0/
	#IN_PATH=root://gfe02.grid.hep.ph.ic.ac.uk//pnfs/hep.ph.ic.ac.uk/data/lz/lz/data/BACCARAT_dev-monash-cbfc0dac_geant10.3/reduced_v6.0.0/
	IN_PATH=/BACCARAT_dev-monash-cbfc0dac_geant10.3/reduced_v6.0.0/
	#COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_U238_g4decay" "lz_vessels_Co60_g4decay")
	#COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_Co60_g4decay")
	COMPLIST=("lz_vessels_U238_g4decay")
elif [ "$MODEL" == "Livermore" ]; then
	#IN_PATH=/scratch/anilima/UKDC/BACCARAT_release-4.0.0_geant10.3/reduced_v6.0.0/
	#IN_PATH=root://gfe02.grid.hep.ph.ic.ac.uk//pnfs/hep.ph.ic.ac.uk/data/lz/lz/data/BACCARAT_release-4.0.0_geant10.3/reduced_v6.0.0/
	IN_PATH=/BACCARAT_release-4.0.0_geant10.3/reduced_v6.0.0/
	#COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_U238_g4decay" "lz_vessels_Co60_g4decay")
	#COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_Co60_g4decay")
	COMPLIST=("lz_vessels_U238_g4decay")
else
	exit 1
fi


# Print array values in  lines
#echo "COMPonents for "$MODEL " are "
#for COMP in ${COMPLIST[*]}; do
#     echo $COMP
#     mkdir AnaTree_Cut_Results/${MODEL}_UKDC/${COMP}/
#     OUT_PATH=AnaTree_Cut_Results/${MODEL}_UKDC/${COMP}/	
#done
echo "MODEL= " $MODEL
for COMP in ${COMPLIST[*]}; do
	mkdir AnaTree_Cut_Results/${MODEL}_UKDC/${COMP}/
	OUT_PATH=AnaTree_Cut_Results/${MODEL}_UKDC/${COMP}/
	IN=${IN_PATH}/${COMP}/
	OUT=${OUT_PATH}/
	./BG_analysis.exe --units keVee --outfile ${OUT}/${COMP}_Cut "name:${COMP};inputs:${IN}/*.root"
	mv ${COMP}*png ${COMP}*pdf BG_analysis_*.txt ${OUT}/
done

