#!/bin/bash -l

source $(pwd)/../set_env.sh

# List of file in a remote server
# xrdfs gfe02.grid.hep.ph.ic.ac.uk ls /pnfs/hep.ph.ic.ac.uk/data/lz/lz/data/<some directory> >> list_<some directory>.log

# Sctipt not working with Baccarat settings
#module unload ROOT
#module load ROOT/6.08.00

#COMP_LIST="lz_tpc_pmts_Co60_g4decay lz_tpc_pmts_K40_g4decay lz_tpc_pmts_Th232_g4decay lz_tpc_pmts_U238_g4decay lz_vessels_Co60_g4decay lz_vessels_K40_g4decay lz_vessels_Sc46_g4decay lz_vessels_Th232_g4decay lz_vessels_U238_g4decay"
COMP_LIST="lz_vessels_U238_g4decay"
MODEL_LIST="dev-monash-cbfc0dac_geant10.3 release-4.0.0_geant10.3"
#MODEL_LIST="release-4.0.0_geant10.3"

BASEDIR=root://gfe02.grid.hep.ph.ic.ac.uk/pnfs/hep.ph.ic.ac.uk/data/lz/BACCARAT
mkdir -p List
for MODEL in $MODEL_LIST; do
	MODELDIR=${BASEDIR}_${MODEL}/reduced_v6.0.0/
	for COMP in $COMP_LIST; do
		mkdir -p /scratch/anilima/UKDC/BACCARAT_${MODEL}
		mkdir -p /scratch/anilima/UKDC/BACCARAT_${MODEL}/reduced_v6.0.0
		mkdir -p /scratch/anilima/UKDC/BACCARAT_${MODEL}/reduced_v6.0.0/${COMP}

		NEWINPDIR=/scratch/anilima/UKDC/BACCARAT_${MODEL}/reduced_v6.0.0/${COMP}
		#Make the List of Files
		inputList=List/List_${MODEL}_${COMP}.txt
		rm $inputList
		xrdfs gfe02.grid.hep.ph.ic.ac.uk ls /pnfs/hep.ph.ic.ac.uk/data/lz/lz/data/BACCARAT_${MODEL}/reduced_v6.0.0/${COMP}/ | xargs -n 1 basename>>$inputList
		echo "Number of files= "
		wc -l $inputList
		#read -p "Attempting to Import All. Yes or No?" answer
		#case $answer in
		#	[Yy]* );;
		#	[Nn]* )exit;;
		#	* ) echo "\nPlease answer yes or no.";;
		#esac
		while IFS= read -r line
		do
			inpFileName="$line"
			#Copy into scratch
			xrdcp root://gfe02.grid.hep.ph.ic.ac.uk//pnfs/hep.ph.ic.ac.uk/data/lz/lz/data/BACCARAT_${MODEL}/reduced_v6.0.0/${COMP}/${inpFileName} $NEWINPDIR/
			echo "Importing DONE: " ${inpFileName}
		done < "$inputList"
	done
done

