// Apply candidate TDR analysis cuts to reduced analysis tree produced by
// LZSim2AnalysisTree.cc.
// 
// Jim Dobson - 1st Nov 2015
// Modified Athoy Nilima
// Help from Brais Lopez to include UKDC option
// OCT 2019
//g++ -o bg_analysis_UKDC.exe bg_analysis_UKDC.cc `root-config --cflags --glibs`
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

#include "TROOT.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TBranch.h"
#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCutG.h"
#include "TCut.h"
#include "TEntryList.h"
#include "TPaveLabel.h"
#include "TLegend.h"
#include "TObjArray.h"
#include "TChainElement.h"
#include "TRegexp.h"
#include "TSystem.h"

#include "TCanvas.h"
#include "TPaveText.h"
#include "TText.h"

#define DEBUG 0

// struct for component info
struct component_info{
  TString name; // for example lz_tpc_top_pmtbases_U238_g4decay
  TString inputs; // /path/to/input/files\*.root
  double yield; // = 1.0 for DecayChain,  n/s/kg @ 1 mBq/kg for neutrons
  double mass; // kg
  double activity; // mBq/kg
};
void print(component_info info);

int main(int argc, char** argv){

  // TODO: switch to Paolo's style file
  gStyle->SetPalette(1);
  gStyle->SetOptStat("ei");
  gStyle->SetStatY(0.9);
  gStyle->SetStatX(0.9);
  gStyle->SetStatW(0.3);
  gStyle->SetStatH(0.15);
  gStyle->SetPadRightMargin(0.1);
  gStyle->SetPadRightMargin(0.15);
  gStyle->SetTitleOffset(1.3,"y");
  cout << endl;

  //////////////////////////////////////////////////////////
  //   Parse input arguments and extract component lists  //
  //////////////////////////////////////////////////////////

  // flags and optional args
  char * path = NULL; // specify common path for all inputs 
  bool verbose = false;
  char * outfile = NULL;
  bool edepcuts = false;
  bool skinedepcuts = false;
  bool savetree = false;
  bool highenergy = false;
  bool minvariables = false;
  double fiducialR = 999.;
  double fiducialZLo = -999.;
  double fiducialZHi = 999.;
  bool isitUKDC=false;
  //TString ListPath;
  TString units;
  // extract flags from arguments
  TObjArray args;
  for(int iarg = 1; iarg < argc; iarg++){
    TObjString * arg = new TObjString(argv[iarg]);
    if(arg->String().Index("--") == 0){
      if(arg->String().Index(TRegexp("units$")) == 2)
          units = argv[++iarg];
      else if(arg->String().Index(TRegexp("verbose$")) == 2)
        verbose = true;
      else if(arg->String().Index(TRegexp("savetree$")) == 2)
        savetree = true;
      else if(arg->String().Index(TRegexp("path$")) == 2)
        path = argv[++iarg];
      else if(arg->String().Index(TRegexp("outfile$")) == 2)
          outfile = argv[++iarg];
      else if(arg->String().Index(TRegexp("edepcuts$")) == 2)
          edepcuts = true;
      else if(arg->String().Index(TRegexp("skinedepcuts$")) == 2)
	  skinedepcuts = true;
      else if(arg->String().Index(TRegexp("highenergy$")) == 2)
          highenergy = true;
      else if(arg->String().Index(TRegexp("minvariables$")) == 2)
          minvariables = true;
      else if(arg->String().Index(TRegexp("fiducialR$")) == 2)
          fiducialR = atof(argv[++iarg]);
      else if(arg->String().Index(TRegexp("fiducialZLo$")) == 2)
          fiducialZLo = atof(argv[++iarg]);
      else if(arg->String().Index(TRegexp("fiducialZHi$")) == 2)
          fiducialZHi = atof(argv[++iarg]);
      else if(arg->String().Index(TRegexp("UKDC")) == 2)
	  isitUKDC=true;
      //else if(arg->String().Index(TRegexp("List$")) == 2)
      //    ListPath = argv[++iarg];
      else {
        cout << "Unrecognised argument: \'"<< arg->String() << "\', exiting!" << endl;
        exit(1);
      }
    }
    else args.Add(arg);
  }
  cout << endl;
  if(path) cout << "Running with --path = " << path << endl;
  if (units != "keVnr" && units!="keVee") {
      cout << "No/incorrect units supplied, please use --units keVnr/keVee when running script" << endl;
      exit(1);
  }
  else cout << "Running with units " << units << endl;

    // if requested save output tree 
    TFile *out_tree_file = NULL;
    TTree *out_tree = NULL;
    if(savetree){
      out_tree_file = new TFile(Form("%s_bg_analysis_tree.root",outfile),"RECREATE");
      out_tree = new TTree("bg_analysis_tree", "Summary output tree for PLR and plots");   
    }
    Int_t br_iTreeNum = -1;
    Int_t br_iNumEvtsPerTree;
    Long_t br_iSumNumEvtsPerTree;
    Int_t br_iEvtN;
    TObjString * br_cPrimaryParName = new TObjString();
    Float_t br_fPrimaryParX_cm;
    Float_t br_fPrimaryParY_cm;
    Float_t br_fPrimaryParZ_cm;
    Bool_t br_bIsUearly;
    Double_t br_fLXeTime_ns;
    Float_t br_fLXeS1cTot_phe;
    Float_t br_fLXeS1c_phe;
    Float_t br_fLXeS2c_phe;
    Float_t br_fLXeS2_phe;
    Float_t br_LXeEDepER_keV;
    Float_t br_LXeEDepNR_keV;
    Float_t br_fLXeX_cm;
    Float_t br_fLXeY_cm;
    Float_t br_fLXeZ_cm;
    Float_t br_fLXeR_cm;
    Float_t br_fLXeSigmaR_cm;
    Float_t br_fLXeSigmaZ_cm;
    Float_t br_fLXeDeltaZ_cm;
    Float_t br_fRFRS1c_phe;
    Float_t br_fRFRER_keV;
    Float_t br_fRFRNR_keV;
    Double_t br_fSkinTime_ns;
    Float_t br_fSkinEDep_keV;
    Float_t br_fSkinS1_phe;
    Float_t br_fSkinX_cm;
    Float_t br_fSkinY_cm;
    Float_t br_fSkinZ_cm;
    Float_t br_fSkinR_cm;
    Float_t br_fSkinSigmaR_cm;
    Float_t br_fSkinSigmaZ_cm;
    Double_t br_fODTime_ns;
    Double_t br_fODProtonTime_ns;
    Float_t br_fODX_cm;
    Float_t br_fODY_cm;
    Float_t br_fODZ_cm;
    Float_t br_fODSigmaX_cm;
    Float_t br_fODSigmaY_cm;
    Float_t br_fODSigmaZ_cm;
    Float_t br_fODEDep_keV;
    Float_t br_fODProton_keV;
    Bool_t br_bPassROICut;
    Bool_t br_bPassSSCut;
    Bool_t br_bPassSkinCut;
    Bool_t br_bPassODCut;
    Bool_t br_bPassFVCut;
    Bool_t br_bPassCuts;
    Float_t br_fLXeEDepNR_frac800us;
    Float_t br_fLXeEDepER_frac800us;
    Float_t br_fRFREDepNR_frac800us;
    Float_t br_fRFREDepER_frac800us;
    Float_t br_fSkinEDepNR_frac800us;
    Float_t br_fSkinEDepER_frac800us;
    Float_t br_fOD_frac800us;
    Float_t br_fODProton_frac800us;
    if(savetree){
      if(minvariables) { // DO NOT ADD TO THIS LIST! Talk to Kevin O. first!!!!
        out_tree->Branch("iEvtN", &br_iEvtN, "iEvtN/I");
        out_tree->Branch("iSumNumEvtsPerTree", &br_iSumNumEvtsPerTree, "iSumNumEvtsPerTree/L");
        out_tree->Branch("cPrimaryParName", &br_cPrimaryParName);
        out_tree->Branch("bIsUearly", &br_bIsUearly, "bIsUearly/O");
        out_tree->Branch("fPrimaryParX_cm", &br_fPrimaryParX_cm, "fPrimaryParX_cm/F");
        out_tree->Branch("fPrimaryParY_cm", &br_fPrimaryParY_cm, "fPrimaryParY_cm/F");
        out_tree->Branch("fPrimaryParZ_cm", &br_fPrimaryParZ_cm, "fPrimaryParZ_cm/F");
        out_tree->Branch("fLXeEDepER_keV", &br_LXeEDepER_keV, "fLXeEDepER_keV/F");
        out_tree->Branch("fLXeEDepNR_keV", &br_LXeEDepNR_keV, "fLXeEDepNR_keV/F");
        out_tree->Branch("fLXeS1c_phe", &br_fLXeS1c_phe, "fLXeS1c_phe/F");
        out_tree->Branch("fLXeS2c_phe", &br_fLXeS2c_phe, "fLXeS2c_phe/F");
        out_tree->Branch("fLXeS2_phe", &br_fLXeS2_phe, "fLXeS2_phe/F");
        out_tree->Branch("fLXeX_cm", &br_fLXeX_cm, "fLXeX_cm/F");
        out_tree->Branch("fLXeY_cm", &br_fLXeY_cm, "fLXeY_cm/F");
        out_tree->Branch("fLXeZ_cm", &br_fLXeZ_cm, "fLXeZ_cm/F");
        out_tree->Branch("fLXeSigmaZ_cm", &br_fLXeSigmaZ_cm, "fLXeSigmaZ_cm/F");
        out_tree->Branch("fLXeDeltaZ_cm", &br_fLXeDeltaZ_cm, "fLXeDeltaZ_cm/F");
        out_tree->Branch("fSkinEDep_keV", &br_fSkinEDep_keV, "fSkinEDep_keV/F");
        out_tree->Branch("fSkinS1_phe", &br_fSkinS1_phe, "fSkinS1_phe/F");
        out_tree->Branch("fODEDep_keV", &br_fODEDep_keV, "fODEDep_keV/F");
      }
      else {
        out_tree->Branch("iTreeNum", &br_iTreeNum, "iTreeNum/I");
        out_tree->Branch("iNumEvtsPerTree", &br_iNumEvtsPerTree, "iNumEvtsPerTree/I");
        out_tree->Branch("iEvtN", &br_iEvtN, "iEvtN/I");
        out_tree->Branch("iSumNumEvtsPerTree", &br_iSumNumEvtsPerTree, "iSumNumEvtsPerTree/L");
        out_tree->Branch("cPrimaryParName", &br_cPrimaryParName);
        out_tree->Branch("bIsUearly", &br_bIsUearly, "bIsUearly/O");
        out_tree->Branch("fPrimaryParX_cm", &br_fPrimaryParX_cm, "fPrimaryParX_cm/F");
        out_tree->Branch("fPrimaryParY_cm", &br_fPrimaryParY_cm, "fPrimaryParY_cm/F");
        out_tree->Branch("fPrimaryParZ_cm", &br_fPrimaryParZ_cm, "fPrimaryParZ_cm/F");
        out_tree->Branch("cPrimaryParName", &br_cPrimaryParName);
        out_tree->Branch("fLXeTime_ns", &br_fLXeTime_ns, "fLXeTime_ns/D");
        out_tree->Branch("fLXeS1cTot_phe", &br_fLXeS1cTot_phe, "fLXeS1cTot_phe/F");
        out_tree->Branch("fLXeS1c_phe", &br_fLXeS1c_phe, "fLXeS1c_phe/F");
        out_tree->Branch("fLXeS2c_phe", &br_fLXeS2c_phe, "fLXeS2c_phe/F");
        out_tree->Branch("fLXeS2_phe", &br_fLXeS2_phe, "fLXeS2_phe/F");
        out_tree->Branch("fLXeEDepER_keV", &br_LXeEDepER_keV, "fLXeEDepER_keV/F");
        out_tree->Branch("fLXeEDepNR_keV", &br_LXeEDepNR_keV, "fLXeEDepNR_keV/F");
        out_tree->Branch("fLXeEDepNR_frac800us", &br_fLXeEDepNR_frac800us, "fLXeEDepNR_frac800us/F");
        out_tree->Branch("fLXeEDepER_frac800us", &br_fLXeEDepER_frac800us, "fLXeEDepER_frac800us/F");
        out_tree->Branch("fLXeX_cm", &br_fLXeX_cm, "fLXeX_cm/F");
        out_tree->Branch("fLXeY_cm", &br_fLXeY_cm, "fLXeY_cm/F");
        out_tree->Branch("fLXeZ_cm", &br_fLXeZ_cm, "fLXeZ_cm/F");
        out_tree->Branch("fLXeR_cm", &br_fLXeR_cm, "fLXeR_cm/F");
        out_tree->Branch("fLXeSigmaR_cm", &br_fLXeSigmaR_cm, "fLXeSigmaR_cm/F");
        out_tree->Branch("fLXeSigmaZ_cm", &br_fLXeSigmaZ_cm, "fLXeSigmaZ_cm/F");
        out_tree->Branch("fLXeDeltaZ_cm", &br_fLXeDeltaZ_cm, "fLXeDeltaZ_cm/F");
        out_tree->Branch("fRFRS1c_phe", &br_fRFRS1c_phe, "fRFRS1c_phe/F");
        out_tree->Branch("fRFRER_keV", &br_fRFRER_keV, "fRFRER_keV/F");
        out_tree->Branch("fRFRNR_keV", &br_fRFRNR_keV, "fRFRNR_keV/F");
        out_tree->Branch("fRFREDepNR_frac800us", &br_fRFREDepNR_frac800us, "fRFREDepNR_frac800us/F");
        out_tree->Branch("fRFREDepER_frac800us", &br_fRFREDepER_frac800us, "fRFREDepER_frac800us/F");
        out_tree->Branch("fSkinTime_ns", &br_fSkinTime_ns, "fSkinTime_ns/D");
        out_tree->Branch("fSkinEDep_keV", &br_fSkinEDep_keV, "fSkinEDep_keV/F");
        out_tree->Branch("fSkinEDepNR_frac800us", &br_fSkinEDepNR_frac800us, "fSkinEDepNR_frac800us/F");
        out_tree->Branch("fSkinEDepER_frac800us", &br_fSkinEDepER_frac800us, "fSkinEDepER_frac800us/F");
        out_tree->Branch("fSkinS1_phe", &br_fSkinS1_phe, "fSkinS1_phe/F");
        out_tree->Branch("fSkinX_cm",&br_fSkinX_cm, "fSkinX_cm/F");
        out_tree->Branch("fSkinY_cm",&br_fSkinY_cm, "fSkinY_cm/F");
        out_tree->Branch("fSkinZ_cm",&br_fSkinZ_cm, "fSkinZ_cm/F");
        out_tree->Branch("fSkinR_cm",&br_fSkinR_cm, "fSkinR_cm/F");
        out_tree->Branch("fSkinSigmaR_cm",&br_fSkinSigmaR_cm, "fSkinSigmaR_cm/F");
        out_tree->Branch("fSkinSigmaZ_cm",&br_fSkinSigmaZ_cm, "fSkinSigmaZ_cm/F");
        out_tree->Branch("fODTime_ns", &br_fODTime_ns, "fODTime_ns/D");
        out_tree->Branch("fODProtonTime_ns", &br_fODProtonTime_ns, "fODProtonTime_ns/D");
        out_tree->Branch("fODX_cm", &br_fODX_cm, "fODX_cm/F");
        out_tree->Branch("fODY_cm", &br_fODY_cm, "fODY_cm/F");
        out_tree->Branch("fODZ_cm", &br_fODZ_cm, "fODZ_cm/F");
        out_tree->Branch("fODSigmaX_cm", &br_fODSigmaX_cm, "fODSigmaX_cm/F");
        out_tree->Branch("fODSigmaY_cm", &br_fODSigmaY_cm, "fODSigmaY_cm/F");
        out_tree->Branch("fODSigmaZ_cm", &br_fODSigmaZ_cm, "fODSigmaZ_cm/F");
        out_tree->Branch("fODEDep_keV", &br_fODEDep_keV, "fODEDep_keV/F");
        out_tree->Branch("fOD_frac800us", &br_fOD_frac800us, "fOD_frac800us/F");
        out_tree->Branch("fODProton_keV", &br_fODProton_keV, "fODProton_keV/F");
        out_tree->Branch("fODProton_frac800us", &br_fODProton_frac800us, "fODProton_frac800us/F");
        out_tree->Branch("bPassROICut", &br_bPassROICut, "bPassROICut/O");
        out_tree->Branch("bPassSSCut", &br_bPassSSCut, "bPassSSCut/O");
        out_tree->Branch("bPassSkinCut", &br_bPassSkinCut, "bPassSkinCut/O");
        out_tree->Branch("bPassODCut", &br_bPassODCut, "bPassODCut/O");
        out_tree->Branch("bPassFVCut", &br_bPassFVCut, "bPassFVCut/O");
        out_tree->Branch("bPassCuts", &br_bPassCuts, "bPassCuts/O");
      }
    }

    TFile *out_file = new TFile(Form("%s_histos.root",outfile),"RECREATE");
    
    //Define the summed output histograms
    /////////////////////////////////////////
    //         Define the histograms       //
    /////////////////////////////////////////
    double dz     = 1;
    double min_z  = -20.;
    double max_z  = 160.;
    int nbin_z = (max_z - min_z)/dz;
    double min_r  = 0;
    double max_r  = 75.;
    double dr2 = 100;
    double max_r2 = max_r*max_r;
    int nbin_r2 = (max_r2 - min_r)/dr2;
    TH2D* h2d_ctsrrz_lxe_summed = new TH2D("h2d_ctsrrz_lxe_summed", "All Energy LXe; r^{2} [cm^{2}]; z [cm]",
                                    nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
    TH2D* h2d_ctsrrz_roi_summed = new TH2D("h2d_ctsrrz_roi_summed", "ROI; r^{2} [cm^{2}]; z [cm]",
                                    nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
    TH2D* h2d_ctsrrz_ss_summed = new TH2D("h2d_ctsrrz_ss_summed", "Single scatter; r^{2} [cm^{2}]; z [cm]",
                                   nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
    TH2D* h2d_ctsrrz_od_summed = new TH2D("h2d_ctsrrz_od_summed", "Single scatter + OD; r^{2} [cm^{2}]; z [cm]",
                                   nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
    TH2D* h2d_ctsrrz_skin_summed = new TH2D("h2d_ctsrrz_skin_summed", "Single scatter + Skin; r^{2} [cm^{2}]; z [cm]",
                                     nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
    TH2D* h2d_ctsrrz_od_skin_summed = new TH2D("h2d_ctsrrz_od_skin_summed", "Single scatter + vetoes; r^{2} [cm^{2}]; z [cm]",
                                    nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
    TH2D* h2d_ctsrrz_fv_summed = new TH2D("h2d_ctsrrz_fv_summed", "5.6t fiducial; r^{2} [cm^{2}]; z [cm]",
                                   nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);

    
    /// Energy Deposition ///
    double ded     = 1.;
    double max_ed  = 3000;
    double min_ed  = 0;
    int    nbin_ed = (max_ed - min_ed)/ded;
    TH1D *h1d_enedep_lxe_summed = new TH1D("h1d_enedep_lxe_summed",
        "Edep in LXe; Er [keV_{ee}]; cts /kg /day",nbin_ed,min_ed, max_ed);
    TH1D *h1d_enedep_od_summed = new TH1D("h1d_enedep_od_summed",
        "Edep in OD; Er [keV_{ee}]; cts /kg /day", nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_skin_summed = new TH1D("h1d_enedep_skin_summed",
        "Edep in LXe Skin; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_ss_summed = new TH1D("h1d_enedep_lxe_ss_summed",
        "Edep in LXe - Single; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_summed = new TH1D("h1d_enedep_lxe_skin_summed",
        "Edep in LXe - Skin; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_od_summed = new TH1D("h1d_enedep_lxe_od_summed",
        "Edep in LXe - OD; Er [keV_{ee}]; cts /kg /day", nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_od_summed = new TH1D("h1d_enedep_lxe_skin_od_summed",
        "Edep in LXe - Skin & OD; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_od_5cm_summed = new TH1D("h1d_enedep_lxe_skin_od_5cm_summed",
        "Edep in LXe - Skin & OD, 5 cm; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_od_10cm_summed = new TH1D("h1d_enedep_lxe_skin_od_10_cm_summed",
        "Edep in LXe - Skin & OD, 10 cm; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_od_15cm_summed = new TH1D("h1d_enedep_lxe_skin_od_15cm_summed",
        "Edep in LXe - Skin & OD, 15 cm; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_od_40cm_summed = new TH1D("h1d_enedep_lxe_skin_od_40cm_summed",
        "Edep in LXe - Skin & OD, 40 cm; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);
    TH1D *h1d_enedep_lxe_skin_od_fv_summed = new TH1D("h1d_enedep_lxe_skin_od_fv_summed",
        "Edep in LXe - Skin & OD, Fid.Vol; Er [keV_{ee}]; cts /kg /day",nbin_ed, min_ed, max_ed);

    
    double dpe = 1.;
    double max_pe  = 10000;
    double min_pe  = 0;
    int    nbin_pe = (max_pe - min_pe)/dpe;
    TH1D *h1d_primary_ene = new TH1D("h1d_primary_ene_summed", "Primary Particle Energy; keV; cts",nbin_pe, min_pe, max_pe);
    
    double devts    = 1.;
    double min_evts = 0.;
    double max_evts = 6.;
    int nbin_evts = (max_evts - min_evts)/devts;
    TH1D *h1d_evts_fv_average = new TH1D("h1d_evts_fv_average",
            "ER Event Survival; ; survival probability", nbin_evts, min_evts, max_evts);

    
    
    
  // loop over components and fill array of component_info structs
  vector<component_info> components;
  vector<TString> ListPathV; 
  for(int iarg = 0; iarg < args.GetEntries(); iarg++){
    // TODO: add option to read from xml file
    TString arg(((TObjString *)args.At(iarg))->String());
    component_info component;  
    component.mass = 1.0;
    component.activity = 1.0;   
    component.yield = 1.0;   
    cout << "\nAdding:"<< endl;
    TObjArray * fields = arg.Tokenize(";"); 
    for(int ifield = 0; ifield < fields->GetEntries(); ifield++){
      TString field = ((TObjString *)(fields->At(ifield)))->String();
      TObjArray *elements = field.Tokenize(":");
      if(elements->GetEntries()==2){
        TString key = ((TObjString *)(elements->At(0)))->String();
        TString value = ((TObjString *)(elements->At(1)))->String();
        if(key == "name") // TODO: check no duplicate names
          component.name = value;
        else if (key == "inputs")
          component.inputs = value;
        else if (key == "mass")
          component.mass = atof(value);
        else if (key == "activity")
          component.activity = atof(value);
        else if (key == "yield")
          component.yield = atof(value);
	else {
          cout << "Unrecognised input field: "<< key <<", exiting!"<< endl;
          exit(1);
        }
      }
      else {
        cout << "Unrecognised input field format: "<< field <<", exiting!"<< endl;
        exit(1);
      }
    } 
    print(component);
    components.push_back(component);
  }
    
    /////////////////////////////////
    //// Define output text file ////
    /////////////////////////////////
    TDatime *timestamp = new TDatime();
    int time = timestamp->GetTime();
    int date = timestamp->GetDate();
    ofstream out;
    out.open(Form("bg_analysis_results_%i_%i.txt",date,time));
    out << "Note that ER roi is 0-100keV, and are scaled to 1.5-6.5keV for the final column. NR roi is 6-30keV." << endl;
    out << endl;
    out << "Outputting information in table format with headings: " << endl;
    out << " | Component & Source  | Total Events  | Yield (for neutrons)  | N Decays |  Xe EDep + ROI |  + Single Hit |  + LXe Skin | + OD  | + OD + LXe Skin  | 5.6t fiducial | P(survival)<br /> /n or g |  P(survival) /decay  | 1000 days @ 1mBq/kg  |" << endl;
    
    
    double pi         = TMath::Pi();
    double rho_lxe    = 2.88/1.e3; // kg/cm^3
    
    double height_tpc = 145.6;    // TPC height cm
    double radius_tpc = 72.8;     // TPC diameter cm
    
    double mass_lxe_tot = rho_lxe * pi * pow(radius_tpc,2) * height_tpc; // kg
    
    double cut_r_5cm    = 5;
    double cut_z_5cm    = 5;
    double mass_lxe_5cm = rho_lxe * pi * pow(radius_tpc - cut_r_5cm,2) * (height_tpc - cut_z_5cm); // kg
    
    double cut_r_10cm    = 10;
    double cut_z_10cm    = 10;
    double mass_lxe_10cm = rho_lxe * pi * pow(radius_tpc - cut_r_10cm,2) * (height_tpc - cut_z_10cm); // kg
    
    double cut_r_15cm    = 15;
    double cut_z_15cm    = 15;
    double mass_lxe_15cm = rho_lxe * pi * pow(radius_tpc - cut_r_15cm,2) * (height_tpc - cut_z_15cm); // kg
    
    double cut_r_40cm    = 40;
    double cut_z_40cm    = 40;
    double mass_lxe_40cm = rho_lxe * pi * pow(radius_tpc - cut_r_40cm,2) * (height_tpc - cut_z_40cm); // kg
    
    double mass_lxe_fv = 5600;

  ////////////////////////
  //    Define cuts     //
  ////////////////////////

    TGraph *fv_cut = new TGraph();
    fv_cut->SetPoint(0, 0, 1.5); // <- 1.5 cm from cathode
    fv_cut->SetPoint(1, 68.8, 1.5); // <- 4 cm from wall: (145.6/2)-4.
    fv_cut->SetPoint(2, 68.8, 132.1); // <- 13.5 cm from gate: 145.6 - 13.5
    fv_cut->SetPoint(3, 0, 132.1);
    
    TGraph *fv_cut_z2r =  new TGraph();
    fv_cut_z2r->SetName("fv_cut_z2r");
    for(int ii = 0; ii < fv_cut->GetN(); ii++) {
        fv_cut_z2r->SetPoint(ii, fv_cut->GetY()[ii],fv_cut->GetX()[ii]);
    }
    TCutG fv_cut_r2("fv_cutr2", 17);
    for(int ipoint=0; ipoint<fv_cut->GetN(); ipoint++){
        fv_cut_r2.SetPoint(ipoint, fv_cut->GetX()[ipoint]*fv_cut->GetX()[ipoint], fv_cut->GetY()[ipoint]);
    }
    
  
  //////////////////////////////////////////////////////
  //    Loop over input components and apply cuts     //
  //////////////////////////////////////////////////////

  // loop over input components
  std::vector<component_info>::iterator component = components.begin();
  cout << "\nLooping over " << components.size() << " components"<< endl;
  int compN=0;
  for(; component != components.end(); component++){
      if (component->activity ==0) {
          cout << "Activity is zero, skipping component..." << endl;
          continue;
      }
    cout << "\nApplying cuts for"<< component->name << endl;
    TString inputs = path != NULL ? Form("%s/%s", path, component->inputs.Data()) : component->inputs;
    TChain *lzsim_analysis = new TChain("lzsim_analysis_tree");
    TChain *header = new TChain("HeaderTree");
    TString Tempinputs="root://gfe02.grid.hep.ph.ic.ac.uk/pnfs/hep.ph.ic.ac.uk/data/lz/lz/data/"+inputs;
 
    if(isitUKDC){
	/*	cout << " ListPath -> "<<ListPath<< endl;
		ifstream inFile;
    		inFile.open(ListPath);
		TString tempfile,Addfile;
		if(!inFile) {
        		cout << "Unable to open file";
        		exit(1); // terminate with error
    		}
		cout<< "Adding File from List: ";
    		while (inFile >>tempfile){
			Addfile=Tempinputs+tempfile;
			cout<<Addfile<<endl;
			header->Add(Addfile);
			lzsim_analysis->Add(Addfile);
    		}
		inFile.close();*/
	cout << "  -> "<< Tempinputs << endl;
        header->Add(Tempinputs);
	lzsim_analysis->Add(Tempinputs);
     }else{
		cout << "  -> "<< inputs << endl;
		 header->Add(inputs);
		lzsim_analysis->Add(inputs);
     }
    Int_t numPrimaries;
    header->SetBranchAddress("iNumPrimaries", &numPrimaries);
      
    int ntrees = lzsim_analysis->GetNtrees();
    cout << "ntrees = " << ntrees;
    bool file_merged_flag = false;//Flag that indicate if the files under processing has been merged or not.
    bool single_entry = true;//Assuming every file has the same number of beam on events
    if(lzsim_analysis->GetEntries() <= 0){
      cout << "Problem with one of input trees, either replace or remove from list of inputs, exiting!" << endl;
      break;
    }
    if(header->GetEntries() != lzsim_analysis->GetNtrees()){
      cout << "Mismatch between number of lzsim_analysis_tree's and entries in header trees, header tree has "<<header->GetEntries()<<" entries. lzsim tree has "<<lzsim_analysis->GetNtrees()<<" trees. File has been merged !"<< endl;
      file_merged_flag = true;
      single_entry = false;
//      exit(1);
    }
      unsigned long nEntries = lzsim_analysis->GetEntries();

      cout << "Loaded " << lzsim_analysis->GetNtrees() << " trees into the TChain, with " << nEntries << " events. " << endl;
      // get largest iPulsesize for all entries
      lzsim_analysis->Draw("iNPrimaryPar>>h_NPrimaryPar");
      TH1D * h_NPrimaryPar = (TH1D*) gDirectory->Get("h_NPrimaryPar");
      int max_NPrimaryPar = (int) h_NPrimaryPar->GetXaxis()->GetBinUpEdge(h_NPrimaryPar->GetNbinsX())+1;
      lzsim_analysis->Draw("iLXeNPulses>>h_LXeNPulses");
      TH1D * h_LXeNPulses = (TH1D*) gDirectory->Get("h_LXeNPulses");
      int max_LXeNPulses = (int) h_LXeNPulses->GetXaxis()->GetBinUpEdge(h_LXeNPulses->GetNbinsX())+1;
      lzsim_analysis->Draw("iSkinNPulses>>h_SkinNPulses");
      TH1D * h_SkinNPulses = (TH1D*) gDirectory->Get("h_SkinNPulses");
      int max_SkinNPulses = (int) h_SkinNPulses->GetXaxis()->GetBinUpEdge(h_SkinNPulses->GetNbinsX())+1;
      lzsim_analysis->Draw("iRFRNPulses>>h_RFRNPulses");
      TH1D * h_RFRNPulses = (TH1D*) gDirectory->Get("h_RFRNPulses");
      int max_RFRNPulses = (int) h_RFRNPulses->GetXaxis()->GetBinUpEdge(h_RFRNPulses->GetNbinsX())+1;
      lzsim_analysis->Draw("iODNPulses>>h_ODNPulses");
      TH1D * h_ODNPulses = (TH1D*) gDirectory->Get("h_ODNPulses");
      int max_ODNPulses = (int) h_ODNPulses->GetXaxis()->GetBinUpEdge(h_ODNPulses->GetNbinsX())+1;
      
      int iEvtN;
      lzsim_analysis->SetBranchAddress("iEvtN", &iEvtN);
      TObjString * cPrimaryParName = NULL;
      lzsim_analysis->SetBranchAddress("cPrimaryParName", &cPrimaryParName);
      float fPrimaryParE_keV[max_NPrimaryPar];
      lzsim_analysis->SetBranchAddress("fPrimaryParE_keV", fPrimaryParE_keV);
      float fPrimaryParX_cm[max_NPrimaryPar];
      lzsim_analysis->SetBranchAddress("fPrimaryParX_cm", fPrimaryParX_cm);
      float fPrimaryParY_cm[max_NPrimaryPar];
      lzsim_analysis->SetBranchAddress("fPrimaryParY_cm", fPrimaryParY_cm);
      float fPrimaryParZ_cm[max_NPrimaryPar];
      lzsim_analysis->SetBranchAddress("fPrimaryParZ_cm", fPrimaryParZ_cm);
      int iLXeNPulses;
      lzsim_analysis->SetBranchAddress("iLXeNPulses",&iLXeNPulses);
      int iLXeNRecords[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("iLXeNRecords",iLXeNRecords);
      double fLXeTime_ns[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeTime_ns",fLXeTime_ns);
      float fLXeX_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeX_cm",fLXeX_cm);
      float fLXeY_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeY_cm",fLXeY_cm);
      float fLXeZ_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeZ_cm",fLXeZ_cm);
      float fLXeR_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeR_cm",fLXeR_cm);
      float fLXeEDepNR_keV[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeEDepNR_keV", fLXeEDepNR_keV);
      float fLXeEDepER_keV[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeEDepER_keV", fLXeEDepER_keV);
     // float fLXeEDepER_keVnr[max_LXeNPulses];
     // lzsim_analysis->SetBranchAddress("fLXeEDepER_keVnr", fLXeEDepER_keVnr);
     // float fLXeEDepNR_keVee[max_LXeNPulses];
     // lzsim_analysis->SetBranchAddress("fLXeEDepNR_keVee",fLXeEDepNR_keVee);
      float fLXeSigmaZ_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeSigmaZ_cm",fLXeSigmaZ_cm);
      float fLXeSigmaR_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeSigmaR_cm",fLXeSigmaR_cm);
      float fLXeDeltaZ_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeDeltaZ_cm",fLXeDeltaZ_cm);
      float fLXeEDepNR_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeEDepNR_frac800us",fLXeEDepNR_frac800us);
      float fLXeEDepER_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeEDepER_frac800us",fLXeEDepER_frac800us);
      
      float fLXeX_nest_weighted_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeX_nest_weighted_cm",fLXeX_nest_weighted_cm);
      float fLXeY_nest_weighted_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeY_nest_weighted_cm",fLXeY_nest_weighted_cm);
      float fLXeZ_nest_weighted_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeZ_nest_weighted_cm",fLXeZ_nest_weighted_cm);
      float fLXeSigmaZ_nest_weighted_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeSigmaZ_nest_weighted_cm",fLXeSigmaZ_nest_weighted_cm);
      float fLXeSigmaR_nest_weighted_cm[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXeSigmaR_nest_weighted_cm",fLXeSigmaR_nest_weighted_cm);
      float fLXe_tot_s1c[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXe_tot_s1c",fLXe_tot_s1c);
      float fLXe_tot_s1[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXe_tot_s1",fLXe_tot_s1);
      float fLXe_tot_s2c[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXe_tot_s2c",fLXe_tot_s2c);
      float fLXe_tot_s2[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fLXe_tot_s2",fLXe_tot_s2);
      
      int iRFRNPulses;
      lzsim_analysis->SetBranchAddress("iRFRNPulses",&iRFRNPulses);
      int iRFRNRecords[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("iRFRNRecords",iRFRNRecords);
      double fRFRTime_ns[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFRTime_ns",fRFRTime_ns);
      float fRFRZ_cm[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFRZ_cm",fRFRZ_cm);
      float fRFRR_cm[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFRR_cm",fRFRR_cm);
      float fRFREDepNR_keV[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFREDepNR_keV", fRFREDepNR_keV);
     // float fRFREDepNR_keVee[max_RFRNPulses];
     // lzsim_analysis->SetBranchAddress("fRFREDepNR_keVee",fRFREDepNR_keVee);
      float fRFREDepER_keV[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFREDepER_keV", fRFREDepER_keV);
     // float fRFREDepER_keVnr[max_RFRNPulses];
     // lzsim_analysis->SetBranchAddress("fRFREDepER_keVnr",fRFREDepER_keVnr);
      float fRFRSigmaZ_cm[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFRSigmaZ_cm",fRFRSigmaZ_cm);
      float fRFRSigmaR_cm[max_RFRNPulses];
      lzsim_analysis->SetBranchAddress("fRFRSigmaR_cm",fRFRSigmaR_cm);
      float fRFREDepNR_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fRFREDepNR_frac800us",fRFREDepNR_frac800us);
      float fRFREDepER_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fRFREDepER_frac800us",fRFREDepER_frac800us);
      
      
      float fRFR_tot_s1c[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fRFR_tot_s1c",fRFR_tot_s1c);
      float fRFR_tot_s1[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fRFR_tot_s1",fRFR_tot_s1);
      
      int iSkinNPulses;
      lzsim_analysis->SetBranchAddress("iSkinNPulses",&iSkinNPulses);
      int iSkinNRecords[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("iSkinNRecords",iSkinNRecords);
      double fSkinTime_ns[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinTime_ns",fSkinTime_ns);
      float fSkin_keV[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkin_keV",fSkin_keV);
      float fSkinX_cm[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinX_cm",fSkinX_cm);
      float fSkinY_cm[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinY_cm",fSkinY_cm);
      float fSkinZ_cm[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinZ_cm",fSkinZ_cm);
      float fSkinR_cm[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinR_cm",fSkinR_cm);
      float fSkinSigmaZ_cm[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinSigmaZ_cm",fSkinSigmaZ_cm);
      float fSkinSigmaR_cm[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkinSigmaR_cm",fSkinSigmaR_cm);
      float fSkin_tot_s1[max_SkinNPulses];
      lzsim_analysis->SetBranchAddress("fSkin_tot_s1",fSkin_tot_s1);
      float fSkinEDepNR_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fSkinEDepNR_frac800us",fSkinEDepNR_frac800us);
      float fSkinEDepER_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fSkinEDepER_frac800us",fSkinEDepER_frac800us);
      
      
      int iODNPulses;
      lzsim_analysis->SetBranchAddress("iODNPulses",&iODNPulses);
      int iODNRecords[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("iODNRecords",iODNRecords);
      double fODTime_ns[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODTime_ns",fODTime_ns);
      double fODProtonTime_ns[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODProtonTime_ns",fODProtonTime_ns);
      float fOD_keV[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fOD_keV",fOD_keV);
      float fODProton_keV[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODProton_keV",fODProton_keV);
      float fODX_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODX_cm",fODX_cm);
      float fODY_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODY_cm",fODY_cm);
      float fODZ_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODZ_cm",fODZ_cm);
      float fODR_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODR_cm",fODR_cm);
      float fODSigmaX_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODSigmaX_cm",fODSigmaX_cm);
      float fODSigmaY_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODSigmaY_cm",fODSigmaY_cm);
      float fODSigmaZ_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODSigmaZ_cm",fODSigmaZ_cm);
      float fODSigmaR_cm[max_ODNPulses];
      lzsim_analysis->SetBranchAddress("fODSigmaR_cm",fODSigmaR_cm);
      float fOD_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fOD_frac800us",fOD_frac800us);
      float fODProton_frac800us[max_LXeNPulses];
      lzsim_analysis->SetBranchAddress("fODProton_frac800us",fODProton_frac800us);
      
      
      
      float fLXeEDepTot;
      float fLXeS1cTot;
//      float fLXeR_nest_weighted_cm[max_LXeNPulses];

      // some proxies to allow switching between energy-deposit- and S1/S2-bases cuts
      float fLXeSigmaR_cm_proxy[max_LXeNPulses];
      float fLXeSigmaZ_cm_proxy[max_LXeNPulses];
      float fLXeR_cm_proxy[max_LXeNPulses];
      float fLXeX_cm_proxy[max_LXeNPulses];
      float fLXeY_cm_proxy[max_LXeNPulses];
      float fLXeZ_cm_proxy[max_LXeNPulses];
      bool skin_cut_proxy; 
      
      bool roi_cut, ss_cut, od_cut, skin_cut, skin_s1_cut;
      bool fiducial_cut;
      bool fiducial_cut_5cm;
      bool fiducial_cut_10cm;
      bool fiducial_cut_15cm;
      bool fiducial_cut_40cm;
      bool save_fiducial_cut;
      /////////////////////////////////////////
      //               ER or NR?             //
      /////////////////////////////////////////
      bool nuclear_recoil = false;
      bool electron_recoil = false;
      double roi_lo, roi_hi;
      double roi_hi_er = 6.5;
      double roi_lo_er = 1.5;
      if (units == "keVnr") {
          cout << "Adapting script for nuclear recoils..." << endl;
          nuclear_recoil = true;
          roi_lo = 6.0;
          roi_hi = 30.0;
      }
      else if (units == "keVee") {
          cout << "Adapting script for electron recoils..." << endl;
          electron_recoil = true;
          roi_lo = 0.0;
          roi_hi = 100.0; // will scale down later
      }
      if(!edepcuts){
        //override roi limits for S1
        roi_lo = 0.;
        roi_hi = 20.;
      }      

      /////////////////////////////////////////
      //         Define the histograms       //
      /////////////////////////////////////////

      TString h_name = Form("h2d_ctsrrz_lxe_%s", component->name.Data());
      TH2D* h2d_ctsrrz_lxe = new TH2D(h_name.Data(), "All Energy LXe; r^{2} [cm^{2}]; z [cm]",
                                      nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      h_name = Form("h2d_ctsrrz_roi_%s", component->name.Data());
      TH2D* h2d_ctsrrz_roi = new TH2D(h_name.Data(), "ROI; r^{2} [cm^{2}]; z [cm]",
                                      nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      h_name = Form("h2d_ctsrrz_ss_%s", component->name.Data());
      TH2D* h2d_ctsrrz_ss = new TH2D(h_name.Data(), "Single scatter; r^{2} [cm^{2}]; z [cm]",
                                     nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      h_name = Form("h2d_ctsrrz_od_%s", component->name.Data());
      TH2D* h2d_ctsrrz_od = new TH2D(h_name.Data(), "Single scatter + OD; r^{2} [cm^{2}]; z [cm]",
                                     nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      h_name = Form("h2d_ctsrrz_skin_%s", component->name.Data());
      TH2D* h2d_ctsrrz_skin = new TH2D(h_name.Data(), "Single scatter + Skin; r^{2} [cm^{2}]; z [cm]",
                                     nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      h_name = Form("h2d_ctsrrz_od_skin_%s", component->name.Data());
      TH2D* h2d_ctsrrz_od_skin = new TH2D(h_name.Data(), "Single scatter + vetoes; r^{2} [cm^{2}]; z [cm]",
                                     nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      h_name = Form("h2d_ctsrrz_fv_%s", component->name.Data());
      TH2D* h2d_ctsrrz_fv = new TH2D(h_name.Data(), "5.6t fiducial; r^{2} [cm^{2}]; z [cm]",
                                     nbin_r2, 0.0, max_r2, nbin_z, min_z, max_z);
      
      /// Energy Deposition ///
        h_name = Form("h1d_enedep_lxe_%s", component->name.Data());
      TH1D *h1d_enedep_lxe = new TH1D(h_name.Data(), Form("Edep in LXe; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_od_%s", component->name.Data());
      TH1D *h1d_enedep_od = new TH1D(h_name.Data(), Form("Edep in OD; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_skin_%s", component->name.Data());
      TH1D *h1d_enedep_skin = new TH1D(h_name.Data(), Form("Edep in LXe Skin; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_ss_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_ss = new TH1D(h_name.Data(), Form("Edep in LXe - Single; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin = new TH1D(h_name.Data(), Form("Edep in LXe - Skin; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_od_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_od = new TH1D(h_name.Data(), Form("Edep in LXe - OD; Er [%s]; cts /kg /day",units.Data()),
                                           nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_od_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin_od = new TH1D(h_name.Data(), Form("Edep in LXe - Skin & OD; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_od_5cm_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin_od_5cm = new TH1D(h_name.Data(), Form("Edep in LXe - Skin & OD, 5 cm; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_od_10_cm_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin_od_10cm = new TH1D(h_name.Data(), Form("Edep in LXe - Skin & OD, 10 cm; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_od_15cm_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin_od_15cm = new TH1D(h_name.Data(), Form("Edep in LXe - Skin & OD, 15 cm; Er [%s]; cts /kg /day",units.Data()),
                                    nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_od_40cm_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin_od_40cm = new TH1D(h_name.Data(), Form("Edep in LXe - Skin & OD, 40 cm; Er [%s]; cts /kg /day",units.Data()),
                                        nbin_ed, min_ed, max_ed);
      h_name = Form("h1d_enedep_lxe_skin_od_fv_%s", component->name.Data());
      TH1D *h1d_enedep_lxe_skin_od_fv = new TH1D(h_name.Data(), Form("Edep in LXe - Skin & OD, Fid.Vol; Er [%s]; cts /kg /day",units.Data()),
                                        nbin_ed, min_ed, max_ed);
      
      /// S1 Spectra ///
      
      double ds1     = 0.5;
      double max_s1  = 100;
      double min_s1  = 0;
      int    nbin_s1 = (max_s1 - min_s1)/ds1;
      h_name = Form("h1d_s1_lxe_%s", component->name.Data());
      TH1D *h1d_s1_lxe = new TH1D(h_name.Data(), "Tot S1c in LXe; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_ss_%s", component->name.Data());
      TH1D *h1d_s1_lxe_ss = new TH1D(h_name.Data(), "Tot S1c in LXe - Single; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_od_%s", component->name.Data());
      TH1D *h1d_s1_lxe_od = new TH1D(h_name.Data(), "Tot S1c in LXe - OD; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_od_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin_od = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin & OD; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_od_5cm_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin_od_5cm = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin & OD, 5 cm; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_od_10_cm_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin_od_10cm = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin & OD, 10 cm; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_od_15cm_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin_od_15cm = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin & OD, 15 cm; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_od_40cm_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin_od_40cm = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin & OD, 40 cm; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_lxe_skin_od_fv_%s", component->name.Data());
      TH1D *h1d_s1_lxe_skin_od_fv = new TH1D(h_name.Data(), "Tot S1c in LXe - Skin & OD, Fid.Vol; S1c [phe]; cts /kg /day", nbin_s1, min_s1, max_s1);
      
      h_name = Form("h1d_s1_skin_%s", component->name.Data());
      TH1D *h1d_s1_skin = new TH1D(h_name.Data(), "Tot S1 in LXe Skin; S1 [phe]; cts", nbin_s1, min_s1, max_s1);
      
      /// S2 Spectra ///
      
      double ds2     = 10;
      double max_s2  = 10000;
      double min_s2  = 0;
      int    nbin_s2 = (max_s2 - min_s2)/ds2;
      h_name = Form("h1d_s2_lxe_%s", component->name.Data());
      TH1D *h1d_s2_lxe = new TH1D(h_name.Data(), "Tot S2c in LXe; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_ss_%s", component->name.Data());
      TH1D *h1d_s2_lxe_ss = new TH1D(h_name.Data(), "Tot S2c in LXe - Single; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_od_%s", component->name.Data());
      TH1D *h1d_s2_lxe_od = new TH1D(h_name.Data(), "Tot S2c in LXe - OD; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_od_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin_od = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin & OD; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_od_5cm_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin_od_5cm = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin & OD, 5 cm; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_od_10_cm_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin_od_10cm = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin & OD, 10 cm; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_od_15cm_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin_od_15cm = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin & OD, 15 cm; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_od_40cm_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin_od_40cm = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin & OD, 40 cm; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      h_name = Form("h1d_s2_lxe_skin_od_fv_%s", component->name.Data());
      TH1D *h1d_s2_lxe_skin_od_fv = new TH1D(h_name.Data(), "Tot S2c in LXe - Skin & OD, Fid.Vol; S2c [phe]; cts /kg /day", nbin_s2, min_s2, max_s2);
      
      /// Discrimination Plots ///
      
      double dlogs2s1     = 0.05;
      double min_logs2s1  = 0.;
      double max_logs2s1  = 4.5;
      int nbin_logs2s1 = (max_logs2s1 - min_logs2s1)/dlogs2s1;
      min_s1  = 0;
      max_s1  = 20;
      ds1 = 0.5;
      nbin_s1 = (max_s1 - min_s1)/ds1;
      h_name = Form("h2d_s1logs2s1_lxe_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_lxe = new TH2D(h_name.Data(), "All Energy LXe; S1c [phe]; log_{10}(S2c/S1c)",
                                      nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      h_name = Form("h2d_s1logs2s1_roi_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_roi = new TH2D(h_name.Data(), "ROI; S1c [phe]; log_{10}(S2c/S1c)",
                                      nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      h_name = Form("h2d_s1logs2s1_ss_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_ss = new TH2D(h_name.Data(), "Single scatter; S1c [phe]; log_{10}(S2c/S1c)",
                                     nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      h_name = Form("h2d_s1logs2s1_od_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_od = new TH2D(h_name.Data(), "Single scatter + OD; S1c [phe]; log_{10}(S2c/S1c)",
                                     nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      h_name = Form("h2d_s1logs2s1_skin_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_skin = new TH2D(h_name.Data(), "Single scatter + Skin; S1c [phe]; log_{10}(S2c/S1c)",
                                     nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      h_name = Form("h2d_s1logs2s1_od_skin_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_od_skin = new TH2D(h_name.Data(), "Single scatter + vetoes; S1c [phe]; log_{10}(S2c/S1c)",
                                     nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      h_name = Form("h2d_s1logs2s1_fv_%s", component->name.Data());
      TH2D* h2d_s1logs2s1_fv = new TH2D(h_name.Data(), "5.6t fiducial; S1c [phe]; log_{10}(S2c/S1c)",
                                     nbin_s1, min_s1, max_s1, nbin_logs2s1, min_logs2s1, max_logs2s1);
      
      double dpe = 1.;
      double max_pe  = 10000;
      double min_pe  = 0;
      int    nbin_pe = (max_pe - min_pe)/dpe;
      h_name = Form("h1d_primary_ene_%s", component->name.Data());
      TH1D *h1d_primary_ene = new TH1D(h_name.Data(), "Primary Particle Energy; keV; cts",nbin_pe, min_pe, max_pe);
      h_name = Form("h1d_evts_fv_%s", component->name.Data());
      TH1D *h1d_evts_fv = new TH1D(h_name.Data(), "Event Survival; ; survival probability", nbin_evts, min_evts, max_evts);
      
     
      
      
    /////////////////////////////////////////
    //    Apply cuts and then make plots   //
    /////////////////////////////////////////
      Double_t rfrContribution_keV = 0.0;
      Int_t cut_level = 0;
      Double_t tenPercent = nEntries/10;
      Double_t nPercent = tenPercent;
      Int_t percent = 10;
      Int_t treenum = -1;
      br_iSumNumEvtsPerTree = 0;
      Int_t total_numPrimaries=0;
      for (unsigned long nEvt= 0; nEvt < nEntries; nEvt++) {
          lzsim_analysis->GetEntry(nEvt);
          int numentries_header = header->GetEntries();
          if(lzsim_analysis->GetTreeNumber() != treenum){
            treenum = lzsim_analysis->GetTreeNumber();
            if (file_merged_flag){// To get the correct number of beam on if there's some files had been merged.
            // Loop the HeaderTree to gather total number of Primaries
                for (int nen=0;nen<numentries_header;nen++){
                	header->GetEntry(nen);
                        br_iNumEvtsPerTree = numPrimaries;
                        br_iTreeNum += 1;
			//If files has been merged, then the number of beam on events per tree is meaningless.Also the total number of tree is meaningless, these two parameters only meaningful when every tree contains the same number of beam on events.
                        total_numPrimaries += numPrimaries;
                }
		br_iSumNumEvtsPerTree = total_numPrimaries;		
		cout<<" Number of beam on --Ryan : "<<total_numPrimaries<<endl;
	        file_merged_flag = false;
	    }
            if (single_entry){//Assuming every file contains the smae number of beam on particles
	    	header->GetEntry(treenum);
           	br_iNumEvtsPerTree = numPrimaries; 
            	br_iTreeNum += 1;
            	// following is useful for keeping track of total number of beamOn
                br_iSumNumEvtsPerTree += numPrimaries;
            
	    }
     } 
          if (nEvt > nPercent)  {
              cout << setprecision(2) << percent << "% of events processed..."  << endl;
              nPercent += tenPercent;
              percent += 10;
          }
          rfrContribution_keV = 0.0;
          cut_level = 0;
          //fill primary particle energy for all evts
          h1d_primary_ene->Fill(fPrimaryParE_keV[0]);
          roi_cut = ss_cut = od_cut = skin_cut = skin_s1_cut = skin_cut_proxy = false; //initialise
          fiducial_cut = fiducial_cut_5cm = fiducial_cut_10cm = fiducial_cut_15cm = fiducial_cut_40cm = false;
          save_fiducial_cut = true;

          br_fLXeS1cTot_phe = br_fLXeS1c_phe = br_fLXeS2c_phe = br_fLXeS2_phe = br_LXeEDepER_keV = br_LXeEDepNR_keV = 0.0;
          br_fLXeX_cm = br_fLXeY_cm = br_fLXeZ_cm = br_fLXeR_cm = 0.0;
          br_fRFRS1c_phe = br_fRFRER_keV = br_fRFRNR_keV = 0.0; 
          br_bPassCuts = br_bPassROICut = br_bPassSSCut = br_bPassSkinCut = br_bPassODCut = br_bPassFVCut = false;
          br_fLXeTime_ns = br_fSkinTime_ns = br_fODTime_ns = br_fODProtonTime_ns = 0.0;
          br_fODX_cm = br_fODY_cm = br_fODZ_cm = 0.0;
          br_fODSigmaX_cm = br_fODSigmaY_cm = br_fODSigmaZ_cm = 0.0;
          br_fODEDep_keV = 0.0;
          br_fODProton_keV = 0.0;
          br_fSkinEDep_keV = br_fSkinS1_phe = br_fSkinX_cm = br_fSkinY_cm = br_fSkinZ_cm = br_fSkinR_cm = br_fSkinSigmaR_cm = br_fSkinSigmaZ_cm = 0.0;
          br_fLXeDeltaZ_cm = br_fLXeSigmaR_cm = br_fLXeSigmaZ_cm = 0.0; 
          br_fRFRS1c_phe = br_fRFRER_keV = br_fRFRNR_keV = 0.0;
          br_fLXeEDepNR_frac800us = br_fLXeEDepER_frac800us = 0.0;
          br_fRFREDepNR_frac800us = br_fRFREDepER_frac800us = 0.0;
          br_fSkinEDepNR_frac800us = br_fSkinEDepER_frac800us = 0.0;
          br_fOD_frac800us = 0.0;
          br_fODProton_frac800us = 0.0;
          br_cPrimaryParName->SetString(cPrimaryParName->GetString().Data());
          br_fPrimaryParX_cm = fPrimaryParX_cm[0];
          br_fPrimaryParY_cm = fPrimaryParY_cm[0];
          br_fPrimaryParZ_cm = fPrimaryParZ_cm[0];
          br_bIsUearly = false; 
          if(cPrimaryParName->GetString().Contains("U238") ||
             cPrimaryParName->GetString().Contains("U234") ||
             cPrimaryParName->GetString().Contains("Th234") ||
             cPrimaryParName->GetString().Contains("Pa234") ||
             cPrimaryParName->GetString().Contains("Th230")){
            br_bIsUearly = true;
          }
          br_iEvtN = iEvtN;

          if (iLXeNPulses > 0) {

              if (nuclear_recoil) { // not going to use keVnr and keVee branches for now because of mistakes in original LZSim2ReducedTree
                  if (iRFRNPulses == 0) {
                      fLXeEDepTot = fLXeEDepNR_keV[0]+fLXeEDepER_keV[0]*(6.0/1.5);
                  }
                  else { // need to include RFR energy
                      // fLXeEDepTot = fLXeEDepNR_keV[0] + fLXeEDepER_keVnr[0] + fRFREDepNR_keV[0] + fRFREDepER_keVnr[0];
                      fLXeEDepTot = fLXeEDepNR_keV[0] + fLXeEDepER_keV[0]*(6.0/1.5) + fRFREDepNR_keV[0] + fRFREDepER_keV[0]*(6.0/1.5);
                      rfrContribution_keV = fRFREDepNR_keV[0] + fRFREDepER_keV[0]*(6.0/1.5); 

                  }
              }
              if (electron_recoil) {
                  if (iRFRNPulses == 0) {
                    //  fLXeEDepTot = fLXeEDepER_keV[0] + fLXeEDepNR_keVee[0];
                      fLXeEDepTot = fLXeEDepER_keV[0] + fLXeEDepNR_keV[0]*(1.5/6.0);
                  }
                  else { // need to include RFR energy
                     // fLXeEDepTot = fLXeEDepER_keV[0] + fLXeEDepNR_keVee[0] + fRFREDepER_keV[0] + fRFREDepNR_keVee[0];
                      fLXeEDepTot = fLXeEDepER_keV[0] + fLXeEDepNR_keV[0]*(1.5/6.0) + fRFREDepER_keV[0] + fRFREDepNR_keV[0]*(1.5/6.0);
                      rfrContribution_keV = fRFREDepER_keV[0] + fRFREDepNR_keV[0]*(1.5/6.0);
                  }
              }
            
              // Switch between energy-deposit- and S1/S2-based cuts  
              if(edepcuts){ 
                fLXeR_cm_proxy[0] = fLXeR_cm[0];
                fLXeX_cm_proxy[0] = fLXeX_cm[0];
                fLXeY_cm_proxy[0] = fLXeY_cm[0];
                fLXeZ_cm_proxy[0] = fLXeZ_cm[0];
                fLXeSigmaR_cm_proxy[0] = fLXeSigmaR_cm[0];
                fLXeSigmaZ_cm_proxy[0] = fLXeSigmaZ_cm[0];
              }
              else{
                fLXeR_cm_proxy[0] = sqrt( pow(fLXeX_nest_weighted_cm[0],2)+pow(fLXeY_nest_weighted_cm[0],2) );
                fLXeX_cm_proxy[0] = fLXeX_nest_weighted_cm[0];
                fLXeY_cm_proxy[0] = fLXeY_nest_weighted_cm[0];
                fLXeZ_cm_proxy[0] = fLXeZ_nest_weighted_cm[0];
                fLXeSigmaR_cm_proxy[0] = fLXeSigmaR_nest_weighted_cm[0];
                fLXeSigmaZ_cm_proxy[0] = fLXeSigmaZ_nest_weighted_cm[0];
              }              

              //Doesn't matter if ER or NR for S1 and S2
              if (iRFRNPulses == 0) {
                 fLXeS1cTot = fLXe_tot_s1c[0];
              }
              else { // need to include RFR S1
                 fLXeS1cTot = fLXe_tot_s1c[0] + fRFR_tot_s1c[0];
              }
              
              if(iODNPulses > 0){ h1d_enedep_od->Fill(fOD_keV[0]); }
              if(iSkinNPulses > 0){ 
                h1d_enedep_skin->Fill(fSkin_keV[0]); 
                h1d_s1_skin->Fill(fSkin_tot_s1[0]);

              }
              h2d_ctsrrz_lxe->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);

              // either use energy-deposit- or S1/S2-based cuts for ROI            
              if(edepcuts){
                if (fLXeEDepTot > roi_lo && fLXeEDepTot < roi_hi) {
                  roi_cut = true;
                }
              }
              else { 
                //total corrected S1 in range & raw S2 greater than 350 phe (5 electrons)  
                if (fLXeS1cTot > roi_lo && fLXeS1cTot < roi_hi && fLXe_tot_s2[0] > 350. ) { 
                  roi_cut = true;
                }
              }

              if (fLXeSigmaR_cm_proxy[0] < 3.0 && fLXeSigmaZ_cm_proxy[0] < 0.2) {
                 ss_cut = true;
              }
              if (iODNPulses > 0 && fOD_keV[0] > 200.0 && TMath::Abs(fODTime_ns[0]-fLXeTime_ns[0]) < 500E3) { //energy based, 200 keV in OD
                 od_cut = true;
              }
              
              if (iSkinNPulses > 0 && fSkin_keV[0] > 100.0 && TMath::Abs(fSkinTime_ns[0]-fLXeTime_ns[0]) < 800E3) { //energy based, 100 keV in skin
                 skin_cut = true;
              }
              if (iSkinNPulses > 0 && fSkin_tot_s1[0] > 3. && TMath::Abs(fSkinTime_ns[0]-fLXeTime_ns[0]) < 800E3) { //S1 based, 3 phe threshold in skin
                 skin_s1_cut = true;
              }
              if(edepcuts || skinedepcuts){ 
                skin_cut_proxy = skin_cut;
              } else {
                skin_cut_proxy = skin_s1_cut;
              }

              if ( (fLXeR_cm_proxy[0] < radius_tpc - cut_r_5cm )&& (fLXeZ_cm_proxy[0] > cut_z_5cm/2 && fLXeZ_cm_proxy[0] < height_tpc-cut_z_5cm/2)) {
                 fiducial_cut_5cm = true;
              }
              if ( (fLXeR_cm_proxy[0] < radius_tpc - cut_r_10cm )&& (fLXeZ_cm_proxy[0] > cut_z_10cm/2 && fLXeZ_cm_proxy[0] < height_tpc-cut_z_10cm/2)){
                 fiducial_cut_10cm = true;
              }
              if ( (fLXeR_cm_proxy[0] < radius_tpc - cut_r_15cm)&& (fLXeZ_cm_proxy[0] > cut_z_15cm/2 && fLXeZ_cm_proxy[0] < height_tpc-cut_z_15cm/2)){
                 fiducial_cut_15cm = true;
              }
              if ( (fLXeR_cm_proxy[0] < radius_tpc - cut_r_40cm) && (fLXeZ_cm_proxy[0] > cut_z_40cm/2 && fLXeZ_cm_proxy[0] < height_tpc-cut_z_40cm/2)){
                 fiducial_cut_40cm = true;
              }
            if (fLXeR_cm_proxy[0] < 68.8 && fLXeZ_cm_proxy[0] > 1.5 && fLXeZ_cm_proxy[0] < 132.1) {
                fiducial_cut = true;
            }
            if (fiducialR < 900. || fiducialZLo > -900. || fiducialZHi < 900.) {
              if (sqrt(fLXeX_cm_proxy[0]*fLXeX_cm_proxy[0] + fLXeY_cm_proxy[0]*fLXeY_cm_proxy[0]) < fiducialR && fLXeZ_cm_proxy[0] > fiducialZLo && fLXeZ_cm_proxy[0] < fiducialZHi) {
                  save_fiducial_cut = true;
              }
              else {
                  save_fiducial_cut = false;
              }
            }
              
              // Fill edep, S1 and S2
              h1d_enedep_lxe->Fill(fLXeEDepTot);
              h1d_s1_lxe->Fill( fLXeS1cTot );
              h1d_s2_lxe->Fill( fLXe_tot_s2c[0] );
              if (ss_cut) {
                  h1d_enedep_lxe_ss->Fill(fLXeEDepTot);
                  h1d_s1_lxe_ss->Fill( fLXeS1cTot );
                  h1d_s2_lxe_ss->Fill( fLXe_tot_s2c[0] );
                  if (!od_cut) {
                      h1d_enedep_lxe_od->Fill(fLXeEDepTot);
                      h1d_s1_lxe_od->Fill( fLXeS1cTot );
                      h1d_s2_lxe_od->Fill( fLXe_tot_s2c[0] );
                  }
                  if (!skin_cut_proxy) {
                      h1d_enedep_lxe_skin->Fill(fLXeEDepTot);
                      h1d_s1_lxe_skin->Fill( fLXeS1cTot );
                      h1d_s2_lxe_skin->Fill( fLXe_tot_s2c[0] );
                  }
                  if (!od_cut && !skin_cut_proxy) {
                      h1d_enedep_lxe_skin_od->Fill(fLXeEDepTot);
                      h1d_s1_lxe_skin_od->Fill( fLXeS1cTot );
                      h1d_s2_lxe_skin_od->Fill( fLXe_tot_s2c[0] );
                      if (fiducial_cut_5cm) {
                          h1d_enedep_lxe_skin_od_5cm->Fill(fLXeEDepTot);
                          h1d_s1_lxe_skin_od_5cm->Fill( fLXeS1cTot );
                          h1d_s2_lxe_skin_od_5cm->Fill( fLXe_tot_s2c[0] );
                      }
                      if (fiducial_cut_10cm) {
                          h1d_enedep_lxe_skin_od_10cm->Fill(fLXeEDepTot);
                          h1d_s1_lxe_skin_od_10cm->Fill( fLXeS1cTot );
                          h1d_s2_lxe_skin_od_10cm->Fill( fLXe_tot_s2c[0] );
                      }
                      if (fiducial_cut_15cm) {
                          h1d_enedep_lxe_skin_od_15cm->Fill(fLXeEDepTot);
                          h1d_s1_lxe_skin_od_15cm->Fill( fLXeS1cTot );
                          h1d_s2_lxe_skin_od_15cm->Fill( fLXe_tot_s2c[0] );
                      }
                      if (fiducial_cut_40cm) {
                          h1d_enedep_lxe_skin_od_40cm->Fill(fLXeEDepTot);
                          h1d_s1_lxe_skin_od_40cm->Fill( fLXeS1cTot );
                          h1d_s2_lxe_skin_od_40cm->Fill( fLXe_tot_s2c[0] );
                      }
                      if (fiducial_cut) {
                          h1d_enedep_lxe_skin_od_fv->Fill(fLXeEDepTot);
                          h1d_s1_lxe_skin_od_fv->Fill( fLXeS1cTot );
                          h1d_s2_lxe_skin_od_fv->Fill( fLXe_tot_s2c[0] );
                      }
                  }
              }
            if (roi_cut) {
                h2d_ctsrrz_roi->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);
                h2d_s1logs2s1_roi->Fill( fLXeS1cTot , log10(fLXe_tot_s2c[0]/fLXeS1cTot) );
                if (ss_cut) {
                      h2d_ctsrrz_ss->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);
                      h2d_s1logs2s1_ss->Fill( fLXeS1cTot , log10(fLXe_tot_s2c[0]/fLXeS1cTot) );
                      if (!od_cut){
                         h2d_ctsrrz_od->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);
                         h2d_s1logs2s1_od->Fill( fLXeS1cTot , log10(fLXe_tot_s2c[0]/fLXeS1cTot) );
                      }
                      if (!skin_cut_proxy){
                         h2d_ctsrrz_skin->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);
                         h2d_s1logs2s1_skin->Fill( fLXeS1cTot , log10(fLXe_tot_s2c[0]/fLXeS1cTot) );
                      }
                      if (!od_cut && !skin_cut_proxy) {
                          h2d_ctsrrz_od_skin->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);
                          h2d_s1logs2s1_od_skin->Fill( fLXeS1cTot , log10(fLXe_tot_s2c[0]/fLXeS1cTot) );
                          if (fiducial_cut){
                             h2d_ctsrrz_fv->Fill(fLXeR_cm_proxy[0]*fLXeR_cm_proxy[0],fLXeZ_cm_proxy[0]);
                             h2d_s1logs2s1_fv->Fill( fLXeS1cTot , log10(fLXe_tot_s2c[0]/fLXeS1cTot) );
                             br_bPassCuts = true;
                          }
                      }
                  
                  }

              //Apply in different order to get same histo as old analysis
                    h1d_evts_fv->Fill(0.);
                    if (fiducial_cut) {
                        cut_level = 1;                         
                        h1d_evts_fv->Fill(1.);
                        if (ss_cut) {
                            cut_level = 2;
                            h1d_evts_fv->Fill(2.);
                            if (!skin_cut_proxy){ 
                              cut_level = 3;
                              h1d_evts_fv->Fill(3.);
                            }  
                            if (!od_cut){
                              cut_level = 4;
                              h1d_evts_fv->Fill(4.);
                            }
                            if (!skin_cut_proxy && !od_cut){
                              cut_level = 5;
                              h1d_evts_fv->Fill(5.);		  
                            }
                        }
                    }
                }

            // print a one line summary of event info for debugging and comparison
            // with old analysis scripts, needs to be updated to include S1/S2 info
            if(verbose){
              printf("%d %d ", iEvtN, cut_level);
              printf("LXe[%0.3f keV (%0.3f keV in RFR) %0.0f ns r=%0.3f +/- %0.3f cm z=%0.3f +/- %0.3f cm] ", fLXeEDepTot,
                      rfrContribution_keV, fLXeTime_ns[0], fLXeR_cm_proxy[0], fLXeSigmaR_cm[0], fLXeZ_cm_proxy[0], fLXeSigmaZ_cm[0]);
              printf("Skin[%d pulses: %0.3f keV %0.0f ns] ", iSkinNPulses, iSkinNPulses > 0 ? fSkin_keV[0] : 0, iSkinNPulses > 0 ? (fSkinTime_ns[0]-fLXeTime_ns[0]) : 0);
              printf("OD[%d pulses: %0.3f keV %0.0f ns]\n", iODNPulses, iODNPulses > 0 ? fOD_keV[0] : 0, iODNPulses > 0 ? (fODTime_ns[0]-fLXeTime_ns[0]) : 0);
            }

            // only save events to summary tree if they pass a loose-selection
            float fLXeEDepTotal_keVee = fLXeEDepER_keV[0] + fLXeEDepNR_keV[0]*(1.5/6.0);
            if(iRFRNPulses > 0){
              fLXeEDepTotal_keVee += fRFREDepER_keV[0];
              fLXeEDepTotal_keVee += fRFREDepNR_keV[0]*(1.5/6.0);
            }
            float max_r_fv_loose = radius_tpc-2.0;
            float max_z_fv_loose = height_tpc-2.0; 
            float min_z_fv_loose = 2.0; 
            float max_roi_loose = 100.0;
            bool fv_loose = fLXeR_cm_proxy[0] < max_r_fv_loose && fLXeZ_cm_proxy[0] > min_z_fv_loose && fLXeZ_cm_proxy[0] < max_z_fv_loose;
            // for now don't apply the loose-fv cut as makes it harder to debug
            if(fLXeEDepTotal_keVee < max_roi_loose || highenergy){ // highenergy bool determines if we want to keep all the events
              br_bPassROICut = roi_cut;
              br_bPassSSCut = ss_cut;
              br_bPassSkinCut = !skin_cut_proxy;
              br_bPassODCut = !od_cut;
              br_bPassFVCut = fiducial_cut;
              if(iLXeNPulses > 0){
                br_fLXeS1cTot_phe = fLXeS1cTot;
                br_fLXeS1c_phe = fLXe_tot_s1c[0];
                br_fLXeS2c_phe = fLXe_tot_s2c[0];
                br_fLXeS2_phe = fLXe_tot_s2[0];
                br_LXeEDepER_keV = fLXeEDepER_keV[0];
                br_LXeEDepNR_keV = fLXeEDepNR_keV[0];
                br_fLXeX_cm = fLXeX_cm_proxy[0];
                br_fLXeY_cm = fLXeY_cm_proxy[0];
                br_fLXeZ_cm = fLXeZ_cm_proxy[0];
                br_fLXeR_cm = TMath::Power(fLXeX_cm_proxy[0]*fLXeX_cm_proxy[0]+fLXeY_cm_proxy[0]*fLXeY_cm_proxy[0], 0.5);
                br_fLXeTime_ns = fLXeTime_ns[0];
                br_fLXeSigmaR_cm = fLXeSigmaR_cm_proxy[0];
                br_fLXeSigmaZ_cm = fLXeSigmaZ_cm_proxy[0];
                br_fLXeDeltaZ_cm = fLXeDeltaZ_cm[0];
                br_fLXeEDepNR_frac800us = fLXeEDepNR_frac800us[0];
                br_fLXeEDepER_frac800us = fLXeEDepER_frac800us[0];
              } 
              if (iRFRNPulses > 0) {
                br_fRFRS1c_phe = fRFR_tot_s1c[0];
                br_fRFRER_keV = fRFREDepER_keV[0];
                br_fRFRNR_keV = fRFREDepNR_keV[0]; 
                br_fRFREDepNR_frac800us = fRFREDepNR_frac800us[0];
                br_fRFREDepER_frac800us = fRFREDepER_frac800us[0];
              }
              if(iSkinNPulses > 0){
                br_fSkinTime_ns = fSkinTime_ns[0];
                br_fSkinS1_phe = fSkin_tot_s1[0];
                br_fSkinEDep_keV = fSkin_keV[0];
		br_fSkinX_cm = fSkinX_cm[0];
		br_fSkinY_cm = fSkinY_cm[0];
		br_fSkinZ_cm = fSkinZ_cm[0];
		br_fSkinR_cm = fSkinR_cm[0];
		br_fSkinSigmaR_cm = fSkinSigmaR_cm[0];
		br_fSkinSigmaZ_cm = fSkinSigmaZ_cm[0];
                br_fSkinEDepNR_frac800us = fSkinEDepNR_frac800us[0];
                br_fSkinEDepER_frac800us = fSkinEDepER_frac800us[0];
              }
              if(iODNPulses > 0){
                br_fODTime_ns = fODTime_ns[0];
                br_fODProtonTime_ns = fODProtonTime_ns[0];
                br_fODEDep_keV = fOD_keV[0];
                br_fODProton_keV = fODProton_keV[0];
                br_fODX_cm = fODX_cm[0];
                br_fODY_cm = fODY_cm[0];
                br_fODZ_cm = fODZ_cm[0];
                br_fODSigmaX_cm = fODSigmaX_cm[0];
                br_fODSigmaY_cm = fODSigmaY_cm[0];
                br_fODSigmaZ_cm = fODSigmaZ_cm[0];
                br_fOD_frac800us = fOD_frac800us[0];
                br_fODProton_frac800us = fODProton_frac800us[0];
              }
              if(savetree && save_fiducial_cut){
                out_tree->Fill();
              }
            } 

          }
      
          else continue;
          
      }
    cout << "...done!" << endl;
    double nbeamon_total = br_iSumNumEvtsPerTree;
    double n_decays = nbeamon_total/(1000*component->yield);
    double livetime = n_decays/(0.001*component->activity*component->mass*24*60*60);
    double roi_scale = 1.;
    if (nuclear_recoil) roi_scale = 1.;
    if (electron_recoil && edepcuts) roi_scale = (roi_hi_er-roi_lo_er)/(roi_hi-roi_lo);
    double scale_factor = (roi_scale/livetime);
    cout << "Total simulated decays is "<< nbeamon_total << "/"<<component->yield << " = "<< n_decays << endl;
    cout << "Activity: " << component->activity << " mBq/kg & component mass: " << component->mass << " kg -> " << livetime << " livedays" << endl;
    if (electron_recoil && edepcuts) h1d_evts_fv->Scale(roi_scale);

    out << "\n | " << component->name << " | " << nbeamon_total << " | " << component->yield << " | " <<  n_decays << " | " << roi_scale*h2d_ctsrrz_roi->GetEntries() << " | " << roi_scale*h2d_ctsrrz_ss->GetEntries() << " | " << roi_scale*h2d_ctsrrz_skin->GetEntries() << " | " << roi_scale*h2d_ctsrrz_od->GetEntries() << " | " << roi_scale*h2d_ctsrrz_od_skin->GetEntries() << " | " << roi_scale*h2d_ctsrrz_fv->GetEntries() << " | " << roi_scale*h2d_ctsrrz_fv->GetEntries()/nbeamon_total << " | " << roi_scale*h2d_ctsrrz_fv->GetEntries()/n_decays << " | " << h2d_ctsrrz_fv->GetEntries()*1000*scale_factor <<  " | \n" << endl;
    out <<"Old cut order: " << endl;
    out << "Events in ROI: " << setprecision(10) << h1d_evts_fv->GetBinContent(1)<< endl;
    out << "Events in ROI + fiducial: " << setprecision(10) << h1d_evts_fv->GetBinContent(2)<< endl;
    out << "Events in ROI + fiducial + single: " << h1d_evts_fv->GetBinContent(3)<< endl;
    out << "Events in ROI + fiducial + single + skin: " << h1d_evts_fv->GetBinContent(4)<< endl;
    out << "Events in ROI + fiducial + single + OD: " << h1d_evts_fv->GetBinContent(5)<< endl;
    out << "Events in ROI + fiducial + single + skin + OD: " << h1d_evts_fv->GetBinContent(6)<< " \n" <<endl;


      
      ///////////////////////////////////
      //        DRAW HISTOGRAMS        //
      ///////////////////////////////////
      
    TString c_name = Form("can_roi_%s", component->name.Data());
    TCanvas * c = new TCanvas(c_name, "", 50, 50, 1100, 700);
    TText *t = new TText(.5,.5,Form("%s, n(beamOn): %.0f",component->name.Data(),nbeamon_total));
    t->SetTextAlign(22);
    t->SetTextColor(kRed+2);
    t->SetTextFont(43);
    t->SetTextSize(10);
    t->Draw();
    c->Divide(3,2);
    c->cd(1);
    h2d_ctsrrz_roi->Scale(scale_factor/mass_lxe_tot);
    h2d_ctsrrz_roi->Draw("colZ1");
    double zmax = h2d_ctsrrz_roi->GetBinContent(h2d_ctsrrz_roi->GetMaximumBin());
    double zmin = 0;
    fv_cut_r2.SetLineColor(kBlack);
    fv_cut_r2.SetLineStyle(kDashed);
    fv_cut_r2.Draw("same");

    c->cd(2); 
    h2d_ctsrrz_ss->Draw("colZ1");
    h2d_ctsrrz_ss->Scale(scale_factor/mass_lxe_tot);
    h2d_ctsrrz_ss->GetZaxis()->SetRangeUser(zmin,zmax);
    fv_cut_r2.Draw("same");

    c->cd(3); 
    h2d_ctsrrz_skin->Draw("colZ1");
    h2d_ctsrrz_skin->Scale(scale_factor/mass_lxe_tot);
    h2d_ctsrrz_skin->GetZaxis()->SetRangeUser(zmin,zmax);
  
    fv_cut_r2.Draw("same");

    c->cd(4);
    h2d_ctsrrz_od->Draw("colZ1");
    h2d_ctsrrz_od->Scale(scale_factor/mass_lxe_tot);
    h2d_ctsrrz_od->GetZaxis()->SetRangeUser(zmin,zmax);
    fv_cut_r2.Draw("same");

    c->cd(5); 
    h2d_ctsrrz_od_skin->Draw("colZ1");
    h2d_ctsrrz_od_skin->Scale(scale_factor/mass_lxe_tot);
    h2d_ctsrrz_od_skin->GetZaxis()->SetRangeUser(zmin,zmax);
    fv_cut_r2.Draw("same");

    c->cd(6); 
    h2d_ctsrrz_fv->Draw("colZ1");
    h2d_ctsrrz_fv->Scale(scale_factor/mass_lxe_fv);
    h2d_ctsrrz_fv->GetZaxis()->SetRangeUser(zmin,zmax);
    fv_cut_r2.Draw("same");
    
    c->Print(Form("%s_bg_analysis_temp.pdf", component->name.Data()));
      
    c_name = Form("can_roi_edep_%s", component->name.Data());
    TCanvas * c2 = new TCanvas(c_name, "", 50, 50, 1100, 700);
    TText *t2 = new TText(.5,.5,Form("%s, n(beamOn): %.0f",component->name.Data(),nbeamon_total));
    t2->SetTextAlign(22);
    t2->SetTextColor(kRed+2);
    t2->SetTextFont(43);
    t2->SetTextSize(10);
    t2->Draw();
      
    c2->Divide(3,2);
    c2->cd(1);
    gPad->SetLogy();
    h1d_enedep_lxe->Draw();
      
      
    c2->cd(2);
    gPad->SetLogy();
    h1d_enedep_lxe_ss->Draw();
    h1d_enedep_lxe_ss->Scale(scale_factor/mass_lxe_tot);
      
    c2->cd(3);
    gPad->SetLogy();
    h1d_enedep_lxe_od->Draw();
    h1d_enedep_lxe_od->Scale(scale_factor/mass_lxe_tot);
      
    c2->cd(4);
    gPad->SetLogy();
    h1d_enedep_lxe_skin->Draw();
    h1d_enedep_lxe_skin->Scale(scale_factor/mass_lxe_tot);
      
    c2->cd(5);
    gPad->SetLogy();
    h1d_enedep_lxe_skin_od->Draw();
    h1d_enedep_lxe_skin_od->Scale(scale_factor/mass_lxe_tot);
 
    c2->cd(6);
    gPad->SetLogy();
    h1d_enedep_lxe_skin_od_fv->Draw();
    h1d_enedep_lxe_skin_od_fv->Scale(scale_factor/mass_lxe_fv);
    
    c2->Print(Form("%s_bg_analysis_edep.pdf", component->name.Data()));
      
    c_name = Form("can_others_%s", component->name.Data());
    TCanvas * c3 = new TCanvas(c_name, "", 50, 50, 1100, 700);
    TText *t3 = new TText(.5,.5,Form("%s, n(beamOn): %.0f",component->name.Data(),nbeamon_total));
    t3->SetTextAlign(22);
    t3->SetTextColor(kRed+2);
    t3->SetTextFont(43);
    t3->SetTextSize(10);
    t3->Draw();
    c3->Divide(3,3);
    c3->cd(1);
    gPad->SetLogy();
    h1d_evts_fv->Draw();
    double nroi = h1d_evts_fv->GetBinContent(1);
    h1d_evts_fv->Scale(1.0/nroi);
    h1d_evts_fv->Draw();
      
    c3->cd(2);
    gPad->SetLogy();
    h1d_enedep_lxe_skin_od->SetLineColor(12);
    h1d_enedep_lxe_skin_od_5cm->SetLineColor(3);
    h1d_enedep_lxe_skin_od_10cm->SetLineColor(7);
    h1d_enedep_lxe_skin_od_15cm->SetLineColor(4);
    h1d_enedep_lxe_skin_od_fv->SetLineColor(6);
    double y_max = 1.1*h1d_enedep_lxe_skin_od->GetMaximum();
    double y_min;
    double y_min_fv = 0.1*h1d_enedep_lxe_skin_od_fv->GetMaximum();
    double y_min_40 = 0.1*h1d_enedep_lxe_skin_od_40cm->GetMaximum();
    if (y_min_40 < y_min_fv && y_min_40>0) y_min = y_min_40;
    else y_min = y_min_fv;
    h1d_enedep_lxe_skin_od->GetXaxis()->SetRangeUser(0, roi_hi);
    h1d_enedep_lxe_skin_od->GetYaxis()->SetRangeUser(y_min,y_max);
    h1d_enedep_lxe_skin_od->Draw();
    h1d_enedep_lxe_skin_od_5cm->Draw("same");
    h1d_enedep_lxe_skin_od_10cm->Draw("same");
    h1d_enedep_lxe_skin_od_15cm->Draw("same");
    h1d_enedep_lxe_skin_od_40cm->Draw("same");
    h1d_enedep_lxe_skin_od_fv->Draw("same");
    h1d_enedep_lxe_skin_od_5cm->Scale(scale_factor/mass_lxe_5cm);
    h1d_enedep_lxe_skin_od_10cm->Scale(scale_factor/mass_lxe_10cm);
    h1d_enedep_lxe_skin_od_15cm->Scale(scale_factor/mass_lxe_15cm);
    h1d_enedep_lxe_skin_od_40cm->Scale(scale_factor/mass_lxe_40cm);
     
    c3->cd(3);
    gPad->SetLogy();
    h1d_primary_ene->Draw();
    
    c3->cd(4);
    h2d_ctsrrz_lxe->Draw("COLZ");
    h2d_ctsrrz_lxe->Scale(scale_factor/mass_lxe_tot);
      
    c3->cd(5);
    gPad->SetLogy();
    h1d_enedep_skin->Draw();

    c3->cd(6);
    gPad->SetLogy();
    h1d_enedep_od->Draw();
    
    c3->cd(7);
    gPad->SetLogy();
    h1d_s1_skin->Draw();
    
    c3->Print(Form("%s_bg_analysis_others.pdf", component->name.Data()));
    c3->Print(Form("%s_bg_analysis_others.C", component->name.Data()));
    // cleanup before moving on to next component
    
    // Discrimination Plots
    c_name = Form("can_disc_%s", component->name.Data());
    TCanvas * c4 = new TCanvas(c_name, "", 50, 50, 1100, 700);
    t = new TText(.5,.5,Form("%s, n(beamOn): %.0f",component->name.Data(),nbeamon_total));
    t->SetTextAlign(22);
    t->SetTextColor(kRed+2);
    t->SetTextFont(43);
    t->SetTextSize(10);
    t->Draw();
    c4->Divide(3,2);
    c4->cd(1);
    h2d_s1logs2s1_roi->Scale(scale_factor/mass_lxe_tot);
    h2d_s1logs2s1_roi->Draw("colZ1");
    zmax = h2d_s1logs2s1_roi->GetBinContent(h2d_s1logs2s1_roi->GetMaximumBin());
    zmin = 0;

    c4->cd(2); 
    h2d_s1logs2s1_ss->Draw("colZ1");
    h2d_s1logs2s1_ss->Scale(scale_factor/mass_lxe_tot);
    h2d_s1logs2s1_ss->GetZaxis()->SetRangeUser(zmin,zmax);

    c4->cd(3); 
    h2d_s1logs2s1_skin->Draw("colZ1");
    h2d_s1logs2s1_skin->Scale(scale_factor/mass_lxe_tot);
    h2d_s1logs2s1_skin->GetZaxis()->SetRangeUser(zmin,zmax);

    c4->cd(4);
    h2d_s1logs2s1_od->Draw("colZ1");
    h2d_s1logs2s1_od->Scale(scale_factor/mass_lxe_tot);
    h2d_s1logs2s1_od->GetZaxis()->SetRangeUser(zmin,zmax);

    c4->cd(5); 
    h2d_s1logs2s1_od_skin->Draw("colZ1");
    h2d_s1logs2s1_od_skin->Scale(scale_factor/mass_lxe_tot);
    h2d_s1logs2s1_od_skin->GetZaxis()->SetRangeUser(zmin,zmax);

    c4->cd(6); 
    h2d_s1logs2s1_fv->Draw("colZ1");
    h2d_s1logs2s1_fv->Scale(scale_factor/mass_lxe_fv);
    h2d_s1logs2s1_fv->GetZaxis()->SetRangeUser(zmin,zmax);
    
    c4->Print(Form("%s_bg_analysis_disc.pdf", component->name.Data()));
    c4->Print(Form("%s_bg_analysis_disc.C", component->name.Data()));
    
    //S1 plots
    c_name = Form("can_roi_s1_%s", component->name.Data());
    TCanvas * c5 = new TCanvas(c_name, "", 50, 50, 1100, 700);
    t2 = new TText(.5,.5,Form("%s, n(beamOn): %.0f",component->name.Data(),nbeamon_total));
    t2->SetTextAlign(22);
    t2->SetTextColor(kRed+2);
    t2->SetTextFont(43);
    t2->SetTextSize(10);
    t2->Draw();
      
      ///////////////////////////////////
      //       SAVING HISTOGRAMS       //
      ///////////////////////////////////
      out_file->cd();
      h2d_ctsrrz_lxe->Write();
      h2d_ctsrrz_roi->Write();
      h2d_ctsrrz_ss->Write();
      h2d_ctsrrz_od->Write();
      h2d_ctsrrz_skin->Write();
      h2d_ctsrrz_od_skin->Write();
      h2d_ctsrrz_fv->Write();
      h1d_enedep_lxe->Write();
      h1d_enedep_od->Write();
      h1d_enedep_skin->Write();
      h1d_enedep_lxe_ss->Write();
      h1d_enedep_lxe_skin->Write();
      h1d_enedep_lxe_od->Write();
      h1d_enedep_lxe_skin_od->Write();
      h1d_enedep_lxe_skin_od_5cm->Write();
      h1d_enedep_lxe_skin_od_10cm->Write();
      h1d_enedep_lxe_skin_od_15cm->Write();
      h1d_enedep_lxe_skin_od_40cm->Write();
      h1d_enedep_lxe_skin_od_fv->Write();
      h1d_primary_ene->Write();
      h1d_evts_fv->Write();
    c5->Divide(3,2);
    c5->cd(1);
    gPad->SetLogy();
    h1d_s1_lxe->Draw();
      
      h2d_ctsrrz_lxe_summed->Add(h2d_ctsrrz_lxe);
      h2d_ctsrrz_roi_summed->Add(h2d_ctsrrz_roi);
      h2d_ctsrrz_ss_summed->Add(h2d_ctsrrz_ss);
      h2d_ctsrrz_od_summed->Add(h2d_ctsrrz_od);
      h2d_ctsrrz_skin_summed->Add(h2d_ctsrrz_skin);
      h2d_ctsrrz_od_skin_summed->Add(h2d_ctsrrz_od_skin);
      h2d_ctsrrz_fv_summed->Add(h2d_ctsrrz_fv);
      h1d_enedep_lxe_summed->Add(h1d_enedep_lxe);
      h1d_enedep_od_summed->Add(h1d_enedep_od);
      h1d_enedep_skin_summed->Add(h1d_enedep_skin);
      h1d_enedep_lxe_ss_summed->Add(h1d_enedep_lxe_ss);
      h1d_enedep_lxe_skin_summed->Add(h1d_enedep_lxe_skin);
      h1d_enedep_lxe_od_summed->Add(h1d_enedep_lxe_od);
      h1d_enedep_lxe_skin_od_summed->Add(h1d_enedep_lxe_skin_od);
      h1d_enedep_lxe_skin_od_5cm_summed->Add(h1d_enedep_lxe_skin_od_5cm);
      h1d_enedep_lxe_skin_od_10cm_summed->Add(h1d_enedep_lxe_skin_od_10cm);
      h1d_enedep_lxe_skin_od_15cm_summed->Add(h1d_enedep_lxe_skin_od_15cm);
      h1d_enedep_lxe_skin_od_40cm_summed->Add(h1d_enedep_lxe_skin_od_40cm);
      h1d_enedep_lxe_skin_od_fv_summed->Add(h1d_enedep_lxe_skin_od_fv);
      h1d_evts_fv_average->Add(h1d_evts_fv);
    c5->cd(2);
    gPad->SetLogy();
    h1d_s1_lxe_ss->Draw();
    h1d_s1_lxe_ss->Scale(scale_factor/mass_lxe_tot);
      
   
      // cleanup before moving on to next component
      h2d_ctsrrz_lxe->Delete();
      h2d_ctsrrz_roi->Delete();
      h2d_ctsrrz_ss->Delete();
      h2d_ctsrrz_od->Delete();
      h2d_ctsrrz_skin->Delete();
      h2d_ctsrrz_od_skin->Delete();
      h2d_ctsrrz_fv->Delete();
      h1d_enedep_lxe->Delete();
      h1d_enedep_od->Delete();
      h1d_enedep_skin->Delete();
      h1d_enedep_lxe_ss->Delete();
      h1d_enedep_lxe_skin->Delete();
      h1d_enedep_lxe_od->Delete();
      h1d_enedep_lxe_skin_od->Delete();
      h1d_enedep_lxe_skin_od_5cm->Delete();
      h1d_enedep_lxe_skin_od_10cm->Delete();
      h1d_enedep_lxe_skin_od_15cm->Delete();
      h1d_enedep_lxe_skin_od_40cm->Delete();
      h1d_enedep_lxe_skin_od_fv->Delete();
      h1d_primary_ene->Delete();
      h1d_evts_fv->Delete();
    c5->cd(3);
    gPad->SetLogy();
    h1d_s1_lxe_od->Draw();
    h1d_s1_lxe_od->Scale(scale_factor/mass_lxe_tot);
      
    c5->cd(4);
    gPad->SetLogy();
    h1d_s1_lxe_skin->Draw();
    h1d_s1_lxe_skin->Scale(scale_factor/mass_lxe_tot);
      
    c5->cd(5);
    gPad->SetLogy();
    h1d_s1_lxe_skin_od->Draw();
    h1d_s1_lxe_skin_od->Scale(scale_factor/mass_lxe_tot);
 
    c5->cd(6);
    gPad->SetLogy();
    h1d_s1_lxe_skin_od_fv->Draw();
    h1d_s1_lxe_skin_od_fv->Scale(scale_factor/mass_lxe_fv);
    
    c5->Print(Form("%s_bg_analysis_s1.pdf", component->name.Data()));
    c5->Print(Form("%s_bg_analysis_s1.C", component->name.Data()));
    
    //S2 plots
    c_name = Form("can_roi_s2_%s", component->name.Data());
    TCanvas * c6 = new TCanvas(c_name, "", 50, 50, 1100, 700);
    t2 = new TText(.5,.5,Form("%s, n(beamOn): %.0f",component->name.Data(),nbeamon_total));
    t2->SetTextAlign(22);
    t2->SetTextColor(kRed+2);
    t2->SetTextFont(43);
    t2->SetTextSize(10);
    t2->Draw();
      
    c6->Divide(3,2);
    c6->cd(1);
    gPad->SetLogy();
    h1d_s2_lxe->Draw();
      
    c6->cd(2);
    gPad->SetLogy();
    h1d_s2_lxe_ss->Draw();
    h1d_s2_lxe_ss->Scale(scale_factor/mass_lxe_tot);
      
    c6->cd(3);
    gPad->SetLogy();
    h1d_s2_lxe_od->Draw();
    h1d_s2_lxe_od->Scale(scale_factor/mass_lxe_tot);
      
    c6->cd(4);
    gPad->SetLogy();
    h1d_s2_lxe_skin->Draw();
    h1d_s2_lxe_skin->Scale(scale_factor/mass_lxe_tot);
      
    c6->cd(5);
    gPad->SetLogy();
    h1d_s2_lxe_skin_od->Draw();
    h1d_s2_lxe_skin_od->Scale(scale_factor/mass_lxe_tot);
 
    c6->cd(6);
    gPad->SetLogy();
    h1d_s2_lxe_skin_od_fv->Draw();
    h1d_s2_lxe_skin_od_fv->Scale(scale_factor/mass_lxe_fv);
    
    c6->Print(Form("%s_bg_analysis_s2.pdf", component->name.Data()));
    c6->Print(Form("%s_bg_analysis_s2.C", component->name.Data()));
    
    delete lzsim_analysis;
    delete header;

  }
    
    double n_scale = h1d_evts_fv_average->GetBinContent(1);
    h1d_evts_fv_average->Scale(1.0/n_scale);
  
    
    out_file->cd();
    TDirectory *summed = out_file->mkdir("Summed");
    summed->cd();
    h2d_ctsrrz_lxe_summed->Write();
    h2d_ctsrrz_roi_summed->Write();
    h2d_ctsrrz_ss_summed->Write();
    h2d_ctsrrz_od_summed->Write();
    h2d_ctsrrz_skin_summed->Write();
    h2d_ctsrrz_od_skin_summed->Write();
    h2d_ctsrrz_fv_summed->Write();
    h1d_enedep_lxe_summed->Write();
    h1d_enedep_od_summed->Write();
    h1d_enedep_skin_summed->Write();
    h1d_enedep_lxe_ss_summed->Write();
    h1d_enedep_lxe_skin_summed->Write();
    h1d_enedep_lxe_od_summed->Write();
    h1d_enedep_lxe_skin_od_summed->Write();
    h1d_enedep_lxe_skin_od_5cm_summed->Write();
    h1d_enedep_lxe_skin_od_10cm_summed->Write();
    h1d_enedep_lxe_skin_od_15cm_summed->Write();
    h1d_enedep_lxe_skin_od_40cm_summed->Write();
    h1d_enedep_lxe_skin_od_fv_summed->Write();
    h1d_evts_fv_average->Write();
    
  out_file->Close();
  out.close();

  if(savetree){
    out_tree_file->cd();
    out_tree->Write();
    out_tree_file->Close();
  }

  return 0;
}

///////////////////////////
//    Useful functions   //
///////////////////////////
 
void print(component_info info){
  cout << "Component "<< info.name << endl;
  cout << " -> inputs  : "<< info.inputs << endl;
  cout << " -> yield   : "<< info.yield << " # progenator decays/# beamon" << endl;
  cout << " -> mass    : "<< info.mass << " kg" << endl;
  cout << " -> activity: "<< info.activity << " mBq/kg" << endl;
}



