COMPLIST=("lz_tpc_pmts_Co60_g4decay" "lz_vessels_K40_g4decay" "lz_tpc_pmts_K40_g4decay" "lz_vessels_Sc46_g4decay" "lz_tpc_pmts_Th232_g4decay" "lz_vessels_Th232_g4decay" "lz_tpc_pmts_U238_g4decay" "lz_vessels_U238_g4decay")

mkdir -p ./Comparison_Plots
#Scaling Factor
inpSIM=4e08
for COMP in ${COMPLIST[*]}; do
        echo $COMP
        root -b -q -l "Macro_Comparison_UKDC.cpp(\"$COMP\",$inpSIM)"
done

