#!/bin/bash
# How to run:  Example: sh G4DecayMacGenerator.sh 50 57 27 Co57_Decay
# or sh G4DecayMacGenerator.sh 50 60 27 Co60_Decay
SEED_NUMBER=$1
SOURCE_A=$2
SOURCE_Z=$3
MACFILE_NAME=$4

MACFILELOC=$(pwd)/Macros/
OUTFILELOC=$(pwd)/Outputs/

GENERATOR=_${SOURCE_A}_${SOURCE_Z}

VOLUME1=LiquidSkinXenon
MASS1=1500
VOLUME2=GaseousSkinXenon
MASS2=1
VOLUME3=LiquidSkinXenonBank
MASS3=500
VOLUME4=LiquidXenonTarget
MASS4=7000
VOLUME5=InnerGaseousXenon
MASS5=1
VOLUME6=ReverseFieldRegion
MASS6=1000

STARTING_SEED=100000
i=0
COUNTER=$STARTING_SEED
CI=$(expr "$SEED_NUMBER" + "$STARTING_SEED")

while [ $COUNTER -lt $CI ]; do
        
	RSEED=$(expr "$COUNTER")
	
	printf "/run/verbose       1 \n"   						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/control/verbose   0 \n"   						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/tracking/verbose  0 \n"   						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/grdm/verbose      0 \n\n" 						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/run/initialize \n\n"      						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/io/outputDir ${OUTFILELOC} \n\n"                     		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/select LZDetector \n" 					>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/LZ/gridWires off \n"     						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/update \n\n"          					>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "# Set Physics: Energy deposit only \n"              			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/physicsList/useOpticalProcesses false \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac	
	printf "# Set Sensitive Volumes record level \n"                      		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "# Record Level 2: energy deposits and multiple scatters \n\n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel LiquidXenonTarget   2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel ReverseFieldRegion  2 \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel LiquidSkinXenon     2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel BottomSkinBank      2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel TopSkinBank         2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel LiquidSkinXenonBank 2 \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel ScintillatorCenter  2 \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "# Set Source Volumes mass \n"                                  		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "/Bacc/detector/setComponentMass ${VOLUME1} ${MASS1} kg \n"   	        >> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "/Bacc/detector/setComponentMass ${VOLUME2} ${MASS2} kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "/Bacc/detector/setComponentMass ${VOLUME3} ${MASS3} kg \n"   		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "/Bacc/detector/setComponentMass ${VOLUME4} ${MASS4} kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "/Bacc/detector/setComponentMass ${VOLUME5} ${MASS5} kg \n"   		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	#printf "/Bacc/detector/setComponentMass ${VOLUME6} ${MASS6} kg \n\n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "# Set Generator for ${ISOTOPE}\n"                            		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/source/set ${VOLUME1} G4Decay${GENERATOR} 1 mBq/kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/source/set ${VOLUME2} G4Decay${GENERATOR} 1 mBq/kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/source/set ${VOLUME3} G4Decay${GENERATOR} 1 mBq/kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/source/set ${VOLUME4} G4Decay${GENERATOR} 1 mBq/kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/source/set ${VOLUME5} G4Decay${GENERATOR} 1 mBq/kg \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/source/set ${VOLUME6} G4Decay${GENERATOR} 1 mBq/kg \n\n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "\n\n # Output .bin files\n"                             		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/io/outputName Out_${MACFILE_NAME}_ \n\n"	>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "\n\n/Bacc/randomSeed ${RSEED}\n\n/Bacc/beamOn 25000\n\nexit\n"       	>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac

        let COUNTER=COUNTER+1
done
