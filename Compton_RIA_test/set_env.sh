#!/bin/bash -l
# Grid Engine options (lines prefixed with #$)
#$ -N Athoy_PMT             
#$ -cwd                  
#$ -l h_rt=24:00:00 
#$ -l s_rt=24:00:00 

#  These options are:
#  job name: -N
#  use the current working directory: -cwd
#  runtime limit of 5 minutes: -l h_rt
#  memory limit of 1 Gbyte: -l h_vmem
# Initialise the environment modules
. /etc/profile.d/modules.sh
#Normal bash commands here:

export BACCDIR=/home/s1642680/LZ_Stuff/BACCARAT
#source $BACCDIR/setup.sh

# ENVIRONMENT VARIABLES
export BACCTOOLS=$BACCDIR/build/x86_64-centos7-gcc7-opt/
export LD_LIBRARY_PATH=$BACCTOOLS/lib/

source $BACCDIR/setup.sh
#export NESTPATH=/global/project/projectdirs/lz/users/anilima/LZsens_PP/fastNEST/libNEST/
export NESTPATH=/cvmfs/lz.opensciencegrid.org/fastNEST/release-5.0.0/libNEST/
export NESTDATA=/cvmfs/lz.opensciencegrid.org/fastNEST/release-5.0.0/LCEAndFieldMaps/
#export NESTDATA=/global/project/projectdirs/lz/users/anilima/fastNEST/LCEAndFieldMaps/ #last / is important
source $NESTPATH/thislibNEST.sh
ROOTPATH=
