#!/bin/bash
# How to run:  Example: sh GammaMacGenerator.sh 500 1 
SEED_NUMBER=$1
ENERGY=$2
MACFILE_NAME=Single_Particle_gamma_${ENERGY}

MACFILELOC=$(pwd)/Macros/
OUTFILELOC=$(pwd)/Outputs/

GENERATOR=

VOLUME1=LiquidXenonTarget
MASS1=7000
STARTING_SEED=100000
i=0
COUNTER=$STARTING_SEED
CI=$(expr "$SEED_NUMBER" + "$STARTING_SEED")

while [ $COUNTER -lt $CI ]; do
        
	RSEED=$(expr "$COUNTER")
	
	printf "/run/verbose       1 \n"   						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/control/verbose   0 \n"   						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/tracking/verbose  0 \n"   						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/grdm/verbose      0 \n\n" 						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/run/initialize \n\n"      						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/io/outputDir ${OUTFILELOC} \n\n"                     		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/select LZDetector \n" 					>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/LZ/gridWires off \n"     						>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/update \n\n"          					>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "# Set Physics: Energy deposit only \n"              			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/physicsList/useOpticalProcesses false \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac	
	printf "# Set Sensitive Volumes record level \n"                      		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "# Record Level 2: energy deposits and multiple scatters \n\n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel LiquidXenonTarget   2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel ReverseFieldRegion  2 \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel LiquidSkinXenon     2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel BottomSkinBank      2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel TopSkinBank         2 \n"   			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel LiquidSkinXenonBank 2 \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/detector/recordLevel ScintillatorCenter  2 \n\n" 			>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "# Set Source Volumes mass \n"                                  		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/detector/setComponentMass ${VOLUME1} ${MASS1} kg \n"   	        >> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/source/set ${VOLUME1} SingleParticle_gamma 1 Bq ${ENERGY}*keV \n" 		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
#	printf "/Bacc/source/set ${VOLUME1} SingleParticle_gamma 1 Bq ${ENERGY}*keV \n"                 >> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "\n\n # Output .bin files\n"                             		>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "/Bacc/io/outputName Out_${MACFILE_NAME}_ \n\n"	>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac
	printf "\n\n/Bacc/randomSeed ${RSEED}\n\n/Bacc/beamOn 25000\n\nexit\n"       	>> ${MACFILELOC}/${MACFILE_NAME}_${RSEED}.mac

        let COUNTER=COUNTER+1
done
