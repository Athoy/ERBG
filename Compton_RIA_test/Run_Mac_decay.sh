#!/bin/bash -l
# Grid Engine options (lines prefixed with #$)
#$ -N Athoy_LZ             
#$ -cwd

#$ -l h_rt=4:00:00 
#$ -l s_rt=4:00:00 

#  These options are:
#  job name: -N
#  use the current working directory: -cwd
#  runtime limit of 5 minutes: -l h_rt
#  memory limit of 1 Gbyte: -l h_vmem
# Initialise the environment modules
. /etc/profile.d/modules.sh
-m beas
#Normal bash commands here:
SEED=$1
NAME=$2

source /home/s1642680/LZ_Stuff/ERBG/Compton_RIA_test/set_env.sh

$BACCDIR/build.x86_64-slc6-gcc48-opt/bin/BACCARATExecutable Macros/${NAME}_${SEED}.mac
$BACCDIR/build.x86_64-slc6-gcc48-opt/bin/BaccRootConverter Outputs/Out_${NAME}_${SEED}.bin
rm ./Outputs/Out_${NAME}_${SEED}.bin


#mkdir -p /scratch/anilima
#mkdir -p /scratch/anilima/Outputs/
#mkdir -p /scratch/anilima/Outputs/${NAME}_${SEED}

#mv /home/s1642680/LZ_Stuff/ERBG/Compton_RIA_test/Outputs/Out_${NAME}_${SEED}.root /scratch/anilima/Outputs/${NAME}_${SEED}/
